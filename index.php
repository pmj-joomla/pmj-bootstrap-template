<!DOCTYPE html>
<?php
defined('_JEXEC') or die;
JHtml::_('jquery.framework');
/*************************************************************/
/* DON'T EDIT THIS FILE UNLESS YOU KNOW WHAT YOU ARE DOING!! */
/*************************************************************/
// add scripts & stylesheets
JHtml::_('script', 'bootstrap.bundle.min.js', array('version' => 'auto', 'relative' => true));
JHtml::_('stylesheet', 'font-awesome.min.css', array('version' => 'auto', 'relative' => true));
JHtml::_('script', 'bootstrap.menuoffscreen.js', array('version' => 'auto', 'relative' => true));
JHtml::_('script', 'prefixfree.min.js', array('version' => 'auto', 'relative' => true));
// load custom js files
if ($this->params->get('customScriptFiles',''))
{
  $jsfiles  = $this->params->get('customScriptFiles','');
  // remove spaces
  $jsfiles  = explode(',',str_replace(' ','',$jsfiles));
  if (is_array($jsfiles))
  {
    foreach ($jsfiles as $jsfile)
    {
      JHtml::_('script', $jsfile, array('version' => 'auto', 'relative' => true));
    }
  }
}
if ($this->params->get('enablePMJ',1))
{
  JLoader::register('PMJTemplate',JPATH_THEMES.'/'.$this->template.'/'.'pmj/libs/template.php');
  $pmj  = new PMJTemplate($this);
  $user = $pmj->user;
  $itemId = Joomla\CMS\Factory::getApplication()->getInput()->getInt('Itemid', 0);
  require_once(JPATH_THEMES.'/'.$this->template.'/pmj/html/template.php');
}
elseif ($this->params->get('enableBootstrap',1))
{
  JLoader::register('PMJBootstrap',JPATH_THEMES.'/'.$this->template.'/'.'pmj/libs/bootstrap.php');
  $bootstrap  = new PMJBootstrap($this);
  $bootstrap->compileBootstrap();
  JHtml::_('stylesheet', 'pmj.min.css', array('version' => 'auto', 'relative' => true));
  require_once(JPATH_THEMES.'/'.$this->template.'/custom_index.php');
}
else
{
  require_once(JPATH_THEMES.'/'.$this->template.'/custom_index.php');
}
/*************************************************************/
/* DON'T EDIT THIS FILE UNLESS YOU KNOW WHAT YOU ARE DOING!! */
/*************************************************************/
