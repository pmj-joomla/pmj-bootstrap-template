<?php
defined('_JEXEC') or die;

JHtml::_('jquery.framework');
$app  = JFactory::getApplication();
$lang = JFactory::getLanguage();
$user = JFactory::getUser();
$params = $app->getTemplate(true)->params;
$isroot = $user->authorise('core.admin');
$customPages  = $params->get('basicError','{}');
$errorcode	= $this->error->getCode();
$currentLang = $lang->getTag();
if (!isset($this->error))
{
	$this->error = JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
	$this->debug = false;
}
?>
<?php
if ($customPages && $errorcode == '404')
{
  header("HTTP/1.0 404 Not Found");
  $customPages	= is_object($customPages) ? $customPages : json_decode($customPages);
  foreach ($customPages as $customPage)
  {
    if ($currentLang == $customPage->basicErrorLang)
    {
      // compose url
      $url  = JURI::root();
      if (!empty($customPage->basicErrorMenuID))
      {
        $menu = $app->getMenu();
        $item = $menu->getItem($customPage->basicErrorMenuID);
        $route  = $item->route;
        $url  = $url.$route;
      }
      elseif (!empty($customPage->basicErrorArticleID))
      {
        $url  = $url.'index.php?option=com_content&view=article&id='.$customPage->basicErrorArticleID;
      }
      elseif (!empty($customPage->basicErrorRoute))
      {
        $url  = $url.$customPage->basicErrorRoute;
      }
      echo file_get_contents($url);
      exit;
    }
    else
    {
      $url  = JURI::root();
      if (!empty($customPages->basicError0->basicErrorMenuID))
      {
        $menu = $app->getMenu();
        $item = $menu->getItem($customPages->basicError0->basicErrorMenuID);
        $route  = $item->route;
        $url  = $url.$route;
      }
      elseif (!empty($customPages->basicError0->basicErrorArticleID))
      {
        $url  = $url.'index.php?option=com_content&view=article&id='.$customPages->basicError0->basicErrorArticleID;
      }
      elseif (!empty($customPages->basicError0->basicErrorRoute))
      {
        $url  = $url.$customPages->basicError0->basicErrorRoute;
      }
      echo file_get_contents($url);
      exit;
    }
  }
} else {
?>
<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<meta charset="utf-8" />
	<title><?php echo $this->error->getCode(); ?> - <?php echo htmlspecialchars($this->error->getMessage(), ENT_QUOTES, 'UTF-8'); ?></title>
	<link href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/css/pmj.min.css" rel="stylesheet" />
	<script src="<?php echo $this->baseurl; ?>/media/jui/js/jquery.min.js" type="text/javascript"></script>
	<script src="<?php echo $this->baseurl; ?>/media/jui/js/jquery-noconflict.js" type="text/javascript"></script>
	<script src="<?php echo $this->baseurl; ?>/media/jui/js/jquery-migrate.min.js" type="text/javascript"></script>
	<script src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/js/bootstrap.bundle.min.js" type="text/javascript"></script>
</head>
<body style="padding:0;">
	<div class="container d-flex flex-column">
		<div class="row justify-content-center frame" style="min-height:100vh;">
			<div class="col-12 col-md-10">
        <div class="card border-primary my-2">
          <div class="card-body">
            <p><strong><?php echo JText::_('JERROR_LAYOUT_NOT_ABLE_TO_VISIT'); ?></strong></p>
            <ol>
              <li><?php echo JText::_('JERROR_LAYOUT_AN_OUT_OF_DATE_BOOKMARK_FAVOURITE'); ?></li>
              <li><?php echo JText::_('JERROR_LAYOUT_SEARCH_ENGINE_OUT_OF_DATE_LISTING'); ?></li>
              <li><?php echo JText::_('JERROR_LAYOUT_MIS_TYPED_ADDRESS'); ?></li>
              <li><?php echo JText::_('JERROR_LAYOUT_YOU_HAVE_NO_ACCESS_TO_THIS_PAGE'); ?></li>
              <li><?php echo JText::_('JERROR_LAYOUT_REQUESTED_RESOURCE_WAS_NOT_FOUND'); ?></li>
              <li><?php echo JText::_('JERROR_LAYOUT_ERROR_HAS_OCCURRED_WHILE_PROCESSING_YOUR_REQUEST'); ?></li>
            </ol>
            <p><strong><?php echo JText::_('JERROR_LAYOUT_PLEASE_TRY_ONE_OF_THE_FOLLOWING_PAGES'); ?></strong></p>
            <ul class="list-unstyled">
              <li><a href="<?php echo JUri::root(true); ?>/index.php" title="<?php echo JText::_('JERROR_LAYOUT_GO_TO_THE_HOME_PAGE'); ?>"><?php echo JText::_('JERROR_LAYOUT_HOME_PAGE'); ?></a></li>
            </ul>
            <p><?php echo JText::_('JERROR_LAYOUT_PLEASE_CONTACT_THE_SYSTEM_ADMINISTRATOR'); ?></p>
          </div>
        </div>
				<div class="card border-danger my-2">
					<div class="card-header bg-danger text-light">
						Error
					</div>
					<div class="card-body">
						<p><?php echo $this->error->getCode(); ?> - <?php echo htmlspecialchars($this->error->getMessage(), ENT_QUOTES, 'UTF-8'); ?></p>
            <?php if ($user->id != 0 && $isroot) : ?>
            <p>
              <code><?php echo htmlspecialchars($this->error->getFile(), ENT_QUOTES, 'UTF-8');?>:<?php echo $this->error->getLine(); ?></code>
            </p>
              <?php echo $this->renderBacktrace(); ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
<?php } ?>