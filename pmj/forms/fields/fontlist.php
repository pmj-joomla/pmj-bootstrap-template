<?php
defined('_JEXEC') or die();

JFormHelper::loadFieldClass('list');

class JFormFieldFontlist extends JFormFieldList
{
	protected $type = 'Fontlist';

	public function getOptions()
	{
		JLoader::register('PMJFont',JPATH_SITE.'/templates/pmjbootstraptemplate/pmj/libs/font.php');
		$path	= JPATH_SITE.'/templates/pmjbootstraptemplate/fonts/custom/';
		$folders = array_diff(scandir($path), array('.', '..','index.html'));
		$extensions	= array('.eot','.svg','.ttf','.woff','.woff2');
		
		$options[0]	= JTEXT::_('JDEFAULT');
		foreach ($folders as $folder)
		{
			if (!is_dir($path.$folder.'/')) continue;
			$font_path	= $path.$folder.'/';
			$files	= array_diff(scandir($font_path), array('.', '..','index.html'));
			$check_file_name	= array();
			foreach ($files as $file)
			{
				// check if all files are available
				$file	= pathinfo($file, PATHINFO_FILENAME);
				$check_file_name[]	= $file;
				foreach ($extensions as $extension)
				{
					if (!file_exists($font_path.$file.$extension))
					{
						continue 3;
					}
				}
			}
			
			// check if all filenames are the same
			$check_file_name	= array_unique($check_file_name);
			if (count($check_file_name) === 1)
			{
				$PMJFont	= new PMJFont;
				$fontname = $PMJFont->get_friendly_ttf_name($font_path.$file.'.ttf')['fontfamily'];
				$options[$folder.'/'.$file]	= $fontname;
			}
		}
		
		return	$options;
	}
}