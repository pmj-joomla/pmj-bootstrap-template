<?php
defined('_JEXEC') or die();

JFormHelper::loadFieldClass('list');

class JFormFieldCustomcolorlist extends JFormFieldList
{
	protected $type = 'Customcolorlist';

	public function getOptions()
	{
		// get current style id
		$app	= JFactory::getApplication();
		$currentStyleID	= $app->input->get->get('id');
		
		// standard options
		$options	= parent::getOptions();
		
		// get custom colors
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('t.params')->from('`#__template_styles` AS t')->where('t.id = "'.$currentStyleID.'" ');
		$rows = $db->setQuery($query)->loadObjectlist();
		$colors	= json_decode($rows[0]->params)->customColors;
		
		// add custom colors to options
		if (is_array($colors) || is_object($colors))
		{
			foreach ($colors as $color)
			{
				$options[$color->custom_color_name]	= $color->custom_color_name;
			}
		}
		
		return	$options;
	}
}