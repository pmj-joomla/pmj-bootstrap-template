<?php
defined('_JEXEC') or die;

class PMJBootstrapTemplateInstallerScript
{
  public function preflight($type, $parent)
  {
    $this->newVersion  = $parent->getManifest()->version;
    if ($type == 'update')
    {
      $this->currentVersion = $this->getParam('version');
    }
  }
  
	public function install($parent)
	{
		// import dependencies
		JLoader::register('JFile', JPATH_LIBRARIES . '/joomla/filesystem/file.php');
		JLoader::register('JFolder', JPATH_LIBRARIES . '/joomla/filesystem/folder.php');
		$templatePath	= JPATH_ROOT.'/templates/pmjbootstraptemplate/';
		$custom_index_file	= $templatePath.'custom_index.php';
		// install custom_index.php if it doesn't exist
		if (!JFile::exists($custom_index_file))
		{
			$customIndex = fopen($custom_index_file, "w");
			fwrite($customIndex, $this->custom_index_content());
			fclose($customIndex);
		}
    $html = '<p><strong>Congratulations!</strong> You have successfully installed PMJ Bootstrap Template '.$this->newVersion.'!</p>';
    $html .= '<p>For detailed information please visit <a href="https://gitlab.com/pmj-joomla/pmj-bootstrap-template" title="PMJ Bootstrap Template GitLab" target="_blank">PMJ Bootstrap Template GitLab</a>!</p>';
    echo $this->informationTemplate('Installation',$html);
	}
	
	public function update($parent)
	{
    $html = '<p><strong>Congratulations!</strong> You have successfully updated PMJ Bootstrap Template from <strong>'.$this->currentVersion.' to '.$this->newVersion.'</strong>!</p>';
		// import dependencies
		JLoader::register('JFile', JPATH_LIBRARIES . '/joomla/filesystem/file.php');
		JLoader::register('JFolder', JPATH_LIBRARIES . '/joomla/filesystem/folder.php');
		$templatePath	= JPATH_ROOT.'/templates/pmjbootstraptemplate/';
		$files	= array();
		$folders	= array();
    $bootstrap  = array();
		
		// file operations based on version
    // 2.1.5
    if (version_compare($this->currentVersion,'2.1.5','<'))
    {
      $files[] = 'css/pmj.min.css';
      $files[] = 'pmj/scss/pmj.scss';
    }
    
    // 2.1.0
    if (version_compare($this->currentVersion,'2.1.0','<'))
    {
      $folders[]	= 'fonts/custom/anton-v9-latin';
      $folders[]	= 'fonts/custom/architects-daughter-v8-latin';
      $folders[]	= 'fonts/custom/audiowide-v6-latin';
      $folders[]	= 'fonts/custom/barrio-v3-latin';
      $folders[]	= 'fonts/custom/butcherman-v10-latin';
      $folders[]	= 'fonts/custom/caveat-brush-v3-latin';
      $folders[]	= 'fonts/custom/caveat-v4-latin';
      $folders[]	= 'fonts/custom/cinzel-decorative-v6-latin';
      $folders[]	= 'fonts/custom/cinzel-v7-latin';
      $folders[]	= 'fonts/custom/cormorant-upright-v4-latin';
      $folders[]	= 'fonts/custom/cormorant-v6-latin';
      $folders[]	= 'fonts/custom/cutive-mono-v6-latin';
      $folders[]	= 'fonts/custom/dosis-v7-latin';
      $folders[]	= 'fonts/custom/ibm-plex-mono-v3-latin';
      $folders[]	= 'fonts/custom/indie-flower-v9-latin';
      $folders[]	= 'fonts/custom/lekton-v8-latin';
      $folders[]	= 'fonts/custom/muli-v11-latin';
      $folders[]	= 'fonts/custom/old-standard-tt-v10-latin';
      $folders[]	= 'fonts/custom/open-sans-condensed-v12-latin';
      $folders[]	= 'fonts/custom/play-v9-latin';
      $folders[]	= 'fonts/custom/poiret-one-v6-latin';
      $folders[]	= 'fonts/custom/press-start-2p-v6-latin';
      $folders[]	= 'fonts/custom/pt-mono-v5-latin';
      $folders[]	= 'fonts/custom/schoolbell-v8-latin';
      $folders[]	= 'fonts/custom/shadows-into-light-two-v5-latin';
      $folders[]	= 'fonts/custom/share-tech-mono-v7-latin';
      $folders[]	= 'fonts/custom/source-code-pro-v8-latin';
      $folders[]	= 'fonts/custom/special-elite-v8-latin';
      $folders[]	= 'fonts/custom/the-girl-next-door-v8-latin';
      $folders[]	= 'fonts/custom/uncial-antiqua-v6-latin';
      $folders[]	= 'fonts/custom/unifrakturcook-v10-latin';
      $folders[]	= 'fonts/custom/unifrakturmaguntia-v8-latin';
      $folders[]	= 'fonts/custom/vt323-v9-latin';
      $folders[]	= 'fonts/custom/zilla-slab-highlight-v5-latin';
      $folders[]	= 'fonts/custom/zilla-slab-v3-latin';
    }

    // 2.0.0
    if (version_compare($this->currentVersion,'2.0.0','<'))
    {
      $files[] = 'html/mod_menu/default.php';
      $files[] = 'html/mod_menu/default_component.php';
      $files[] = 'html/mod_menu/default_heading.php';
      $files[] = 'html/mod_menu/default_separator.php';
      $files[] = 'html/mod_menu/default_url.php';
      $files[] = 'html/mod_menu/navbar-usermenu.php';
      $files[] = 'html/mod_menu/navbar-usermenu_component.php';
      $files[] = 'html/mod_menu/navbar-usermenu_heading.php';
      $files[] = 'html/mod_menu/navbar-usermenu_separator.php';
      $files[] = 'html/mod_menu/navbar-usermenu_url.php';
    }
    
    // 1.4.0
    if (version_compare($this->currentVersion,'1.4.0','<'))
    {
      $custom_index_file	= $templatePath.'custom_index.php';
      // install custom_index.php if it doesn't exist
      if (!JFile::exists($custom_index_file))
      {
        $customIndex = fopen($custom_index_file, "w");
        fwrite($customIndex, $this->custom_index_content());
        fclose($customIndex);
      }
    }
    
		// 0.7.3
    if (version_compare($this->currentVersion,'0.7.3','<'))
    {
      $files[]	= 'js/bootstrap.stickynav.js';
    }
		
		// 0.7.0
    if (version_compare($this->currentVersion,'0.7.0','<'))
    {
      $files[]	= 'css/bootstrap.min.css';
      $files[]	= 'css/normalize.css';
      $files[]	= 'pmj/scss/gradients.scss';
      $folders[]	= 'scss/custom';
    }
		
		// 0.5.0
    if (version_compare($this->currentVersion,'0.5.0','<'))
    {
      $files[]	= 'pmj.php';
    }
		
		// 0.4.0
    if (version_compare($this->currentVersion,'0.4.0','<'))
    {
      $folders[]	= 'compiler';
      $folders[]	= 'form';
      $files[]	= 'libs/fontInfo.php';
    }
		
		// 0.3.0
    if (version_compare($this->currentVersion,'0.3.0','<'))
    {
      $folders[]	= 'scss/scssphp';
      $files[]	= 'libs/ttfInfo.class.php';
    }
		
		// remove files
    $html .= '<ul>';
		foreach ($files as $file)
		{
			if (JFile::exists($templatePath.$file))
			{
				JFile::delete($templatePath.$file);
        $html .= '<li>Deleted File: '.$file.'</li>';
			}
		}
		$html .= '</ul>';
    
		// remove folders
    $html .= '<ul>';
		foreach ($folders as $folder)
		{
			if (JFolder::exists($templatePath.$folder))
			{
				JFolder::delete($templatePath.$folder);
        $html .= '<li>Deleted Directory: '.$folder.'</li>';
			}
		}
    $html .= '</ul>';
    
    echo $this->informationTemplate('Update',$html);
	}
	
	private function custom_index_content()
	{
		$contents	= '<?php'.PHP_EOL;
		$contents	.= 'defined(\'_JEXEC\') or die;'.PHP_EOL;
		$contents	.= '?>'.PHP_EOL;
		$contents	.= '<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">'.PHP_EOL;
		$contents	.= '<head>'.PHP_EOL;
		$contents	.= '	<jdoc:include type="head" />'.PHP_EOL;
		$contents	.= '</head>'.PHP_EOL;
		$contents	.= '<body>'.PHP_EOL;
		$contents	.= 'This is the place to create your own layout'.PHP_EOL;
		$contents	.= '</body>'.PHP_EOL;
		$contents	.= '</html>'.PHP_EOL;
		
		return $contents;
	}
  
  private function getParam($name)
  {
    $db = JFactory::getDbo();
    $db->setQuery('SELECT manifest_cache FROM #__extensions WHERE name = "PMJBootstrapTemplate"');
    $manifest = json_decode( $db->loadResult(), true );
    return $manifest[ $name ];
  }
  
  private function informationTemplate($type=null,$html=null)
  {
    $bootstrap  = $this->informationBootstrap();
    $contents = '<div class="alert alert-info">';
    $contents .= '<h4 class="alert-heading">PMJ Bootstrap Template '.$type.'!</h4>';
    $contents .= $html;
    if ($bootstrap)
    {
      $contents .= '<h5 class="alert-heading">Bootstrap Info</h5>';
      $contents .= $bootstrap;
    }
    $contents .= '</div>';
    return $contents;
  }
  
  private function informationBootstrap()
  {
    $bootstrap  = array();
    
    // 2.0.0
    if (version_compare($this->currentVersion,'2.0.0','<'))
    {
      $bootstrap['Navbar / Navs'][] = '$navbar-dark-toggler-icon-bg';
      $bootstrap['Navbar / Navs'][] = '$navbar-light-toggler-icon-bg';
      $bootstrap['Dropdowns / Buttons'][] = '$dropdown-inner-border-radius';
      $bootstrap['Forms'][] = '$input-height-inner';
      $bootstrap['Forms'][] = '$input-height-inner-half';
      $bootstrap['Forms'][] = '$input-height-inner-quarter';
      $bootstrap['Forms'][] = '$input-height';
      $bootstrap['Forms'][] = '$input-height-sm';
      $bootstrap['Forms'][] = '$input-height-lg';
      $bootstrap['Forms'][] = '$custom-control-indicator-checked-box-shadow';
      $bootstrap['Forms'][] = '$custom-control-indicator-active-box-shadow';
      $bootstrap['Forms'][] = '$custom-checkbox-indicator-icon-checked';
      $bootstrap['Forms'][] = '$custom-checkbox-indicator-icon-indeterminate';
      $bootstrap['Forms'][] = '$custom-checkbox-indicator-indeterminate-box-shadow';
      $bootstrap['Forms'][] = '$custom-radio-indicator-icon-checked';
      $bootstrap['Forms'][] = '$custom-switch-indicator-size';
      $bootstrap['Forms'][] = '$custom-select-indicator';
      $bootstrap['Forms'][] = '$custom-select-background';
      $bootstrap['Forms'][] = '$custom-select-feedback-icon-padding-right';
      $bootstrap['Forms'][] = '$form-feedback-icon-valid';
      $bootstrap['Forms'][] = '$form-feedback-icon-invalid';
      $bootstrap['Cards / Tooltips / Popover / Toasts'][] = '$card-inner-border-radius';
      $bootstrap['Modals / List Group / Carousel'][]  = '$carousel-control-prev-icon-bg';
      $bootstrap['Modals / List Group / Carousel'][]  = '$carousel-control-next-icon-bg';
    }
    
    if (!empty($bootstrap))
    {
      $html = '<p>Due to a Bootstrap Update you should check the following variables and revert them to default:</p>';
      $html .= '<ul>';
      foreach ($bootstrap as $section => $variables)
      {
        $html .= '<li>'.$section.'<ul>';
        foreach ($variables as $variable)
        {
          $html .= '<li>'.$variable.'</li>';
          if ($variable === end($variables))
          {
            $html .= '</ul></li>';
          }
        }
      }
      $html .= '</ul>';
      return $html;
    }
    else
    {
      return null;
    }
  }
}