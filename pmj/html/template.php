<?php
defined('_JEXEC') or die;
?>
<?php $fluid	= $this->params->get('static','static') === 'fluid' ? '-fluid' : ''; ?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<jdoc:include type="head" />
	<?php echo $this->params->get('customScriptHead',''); ?>
</head>
<body class="<?php echo 'pmjbody-' . $itemId;?>">
	<?php if ($this->params->get('enableMatomo',0) && $this->params->get('matomoDomain','') && $this->params->get('matomoSite','')) : ?>
	<script type="text/javascript">
		<!-- Matomo -->
		var _paq	= window._paq || [];
		<?php if ($user->id != 0 && $this->params->get('matomoUser',0)) : ?>
		_paq.push(['setUserId', '<?php echo $this->params->get('matomoUser',0) === 'email' ? $user->email : $user->username; ?>']);
		<?php endif; ?>
		<?php if ($this->params->get('matomoSitename','url' === 'sitename')) : ?>
		_paq.push(['setDocumentTitle', document.title]);
		<?php endif; ?>
		<?php if ($this->params->get('matomoHeartbeat',1)) : ?>
		_paq.push(['enableHeartBeatTimer', <?php echo $this->params->get('matomoHeartbeatInterval',15); ?>]);
		<?php endif; ?>
		/* tracker methods like "setCustomDimension" should be called before "trackPageView" */
		_paq.push(['trackPageView']);
		_paq.push(['enableLinkTracking']);
		(function() {
			var u	= "<?php echo $this->params->get('matomoDomain',''); ?>";
			_paq.push(['setTrackerUrl', u+'matomo.php']);
			_paq.push(['setSiteId', '<?php echo $this->params->get('matomoSite','1'); ?>']);
			
			var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
			g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
		})();
		<!-- End Matomo Code -->
	</script>
	<noscript><p><img src="<?php echo $this->params->get('matomoDomain',''); ?>piwik.php?idsite=<?php echo $this->params->get('matomoSite','1'); ?>&amp;rec=1" style="border:0" alt="" /></p></noscript>
	<?php endif; ?>

	<?php if ($this->params->get('enableFontResizer',1)) : ?>
	<div id="sizer" class="d-none d-sm-block ml-1 float-left">
		<button id="font-increase" type="button" class="btn btn-<?php echo $this->params->get('outlineFontResizer',1) ? 'outline-' : '';?><?php echo $this->params->get('colorFontResizer','primary'); ?> btn-sm my-1"><i class="fa fa-search-plus"></i></button><br />
		<button id="font-reset" type="button" class="btn btn-<?php echo $this->params->get('outlineFontResizer',1) ? 'outline-' : '';?><?php echo $this->params->get('colorFontResizer','primary'); ?> btn-sm my-1"><i class="fa fa-refresh"></i></button><br />
		<button id="font-decrease" type="button" class="btn btn-<?php echo $this->params->get('outlineFontResizer',1) ? 'outline-' : '';?><?php echo $this->params->get('colorFontResizer','primary'); ?> btn-sm my-1"><i class="fa fa-search-minus"></i></button>
	</div>
	<?php endif; ?>
	<div class="container<?php echo $fluid; ?> frame d-flex flex-column">
		<?php $pmj->renderTemplate(); ?>
	</div>
	<?php require_once(JPATH_THEMES.'/'.$this->template.'/pmj/html/scripts.php'); ?>
	<jdoc:include type="modules" name="debug" style="none" />
</body>
</html>