<?php
defined('_JEXEC') or die;
?>
<footer class="<?php echo $this->params->get('static','static') === 'static' && $this->params->get('footerBreakout',0) ? 'breakout ' : 'row ' ; ?>footer<?php echo $this->params->get('footerBottom',1) ? ' mt-auto' : ''; ?>">
	<?php echo $this->params->get('static','static') === 'static' && $this->params->get('footerBreakout',0) ? '<div class="container"><div class="row">' : '' ; ?>
	<?php if ($this->template->countModules('footer')) : ?>
		<div class="col-12">
		<jdoc:include type="modules" name="footer" style="none" />
		</div>
	<?php endif; ?>
	<?php if ($footerText) : ?>
		<div class="<?php echo $classText; ?>">
			<?php echo $footerText; ?>
		</div>
	<?php endif; ?>
	<?php if ($this->params->get('footerBacklink',1) || $footerText) : ?>
		<div class="<?php echo $classBacklink; ?>">
			<?php echo $this->params->get('footerBacklink',1) ? '<a href="https://pmj.rocks" title="Template by PMJ"'.($this->menu->getActive() == $this->menu->getDefault() ? ' rel="external"' : ' rel="nofollow"').' target="_blank"><i class="fa fa-paperclip"></i> PMJ</a>' : '<i class="fa fa-paperclip"></i> PMJ'; ?>
		</div>
	<?php endif; ?>
	<?php echo $this->params->get('static','static') === 'static' && $this->params->get('footerBreakout',0) ? '</div></div>' : '' ; ?>
</footer>