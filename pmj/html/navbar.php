<?php
defined('_JEXEC') or die;

$class	= 'navbar';
if ($params->get('navbarPosition',0) && $params->get('navbarPosition',0) === 'top')
{
	$class	.= ' fixed-top';
}
if ($params->get('navbarCollapsebreakpoint','lg'))
{
	$class	.= ' navbar-expand-'.$params->get('navbarCollapsebreakpoint','lg');
}
if ($params->get('navbarStyle','dark'))
{
	$class	.= ' navbar-'.$params->get('navbarStyle','dark');
}
if ($params->get('navbarBackground','primary'))
{
	$class	.= ' bg-'.$params->get('navbarBackground','primary');
}
if (($params->get('navbarPosition',0) !== 'top' && $params->get('navbarWidth','frame') === 'frame') || ($params->get('navbarPosition',0) === 'top' && $params->get('navbarTopWidth','fullcontainer') === 'container'))
{
	$class	.= ' container';
}

?>
<?php if ($params->get('navbarPosition',0) !== 'top') : ?>
<div class="<?php echo $params->get('navbarWidth','frame') === 'frame' ? 'row' : ''; ?>"<?php echo $params->get('navbarStickyonscroll',0) ? ' data-toggle="sticky-onscroll"' : ''; ?>>
<?php endif; ?>
<nav class="<?php echo $class; ?>">
	<?php if ($params->get('navbarTopWidth','fullcontainer') === 'fullcontainer') : ?>
	<div class="container">
	<?php endif; ?>
	<?php if ($params->get('navbarLabel',0)) : ?>
	<a class="navbar-brand" href="/"><?php echo ($params->get('navbarLabel',0)) === 'sitename' ? $this->sitename : (($params->get('navbarLabel',0)) === 'custom' ? ($params->get('navbarLabelText','Navbar Label')) : ''); ?></a>
	<?php endif; ?>
  <jdoc:include type="modules" name="navbar-before" style="none" />
	<?php if ($params->get('navbarCollapsebreakpoint','lg')) : ?>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarPMJ" aria-controls="navbarPMJ" aria-expanded="false" aria-label="<?php echo JText::_('TPL_PMJBOOTSTRAPTEMPLATE_TOGGLE_NAVIGATION'); ?>">
		<span class="navbar-toggler-icon"></span>
	</button>
	<?php endif; ?>
  <div class="collapse navbar-collapse" id="navbarPMJ">
		<jdoc:include type="modules" name="navbar" style="none" />
	</div>
  <jdoc:include type="modules" name="navbar-after" style="none" />
	<?php if ($params->get('navbarTopWidth','fullcontainer') === 'fullcontainer') : ?>
	</div>
	<?php endif; ?>
	
</nav>
<?php if ($params->get('navbarPosition',0) !== 'top') : ?>
</div>
<?php endif; ?>