<?php
defined('_JEXEC') or die;
?>
<script type="text/javascript">
<?php if ($this->params->get('navbarStickyonscroll',0)) : ?>
/* Bootstrap 4 Sticky Nav by https://bootbites.com/articles/freebie-sticky-bootstrap-navbar-scroll-bootstrap-4/ */

jQuery(document).ready(function() {
	/* Custom */
	var stickyToggle = function(sticky, stickyWrapper, scrollElement) {
		var stickyHeight = sticky.outerHeight();
		var stickyTop = stickyWrapper.offset().top;
		if (scrollElement.scrollTop() >= stickyTop)
		{
			stickyWrapper.height(stickyHeight);
			sticky.addClass("is-sticky").addClass('fixed-top')<?php echo $this->params->get('navbarWidth','frame') === 'frame' ? '' : '.addClass(\'container\')'; ?>;
		}
		else
		{
			sticky.removeClass("is-sticky").removeClass('fixed-top')<?php echo $this->params->get('navbarWidth','frame') === 'frame' ? '' : '.removeClass(\'container\')'; ?>;
			stickyWrapper.height('auto');
		}
	};

	/* Find all data-toggle="sticky-onscroll" elements */
	jQuery('[data-toggle="sticky-onscroll"]').each(function() {
		var sticky = jQuery(this);
		var stickyWrapper = jQuery('<div>').addClass('sticky-wrapper'); /* insert hidden element to maintain actual top offset on page */
		sticky.before(stickyWrapper);
		sticky.addClass('sticky');

		/* Scroll & resize events */
		jQuery(window).on('scroll.sticky-onscroll resize.sticky-onscroll', function() {
			stickyToggle(sticky, stickyWrapper, jQuery(this));
		});

		/* On page load */
		stickyToggle(sticky, stickyWrapper, jQuery(window));
	});
});
<?php endif; ?>

jQuery(function () {
	jQuery('[data-toggle="tooltip"]').tooltip()
})
<?php if ($this->params->get('enableFontResizer',1)) : ?>
jQuery('#font-increase').on('click', function() {
	var size	= parseInt(jQuery('html').css('font-size')) + 1;
	jQuery('html').css("font-size", size + "px");
});
jQuery('#font-decrease').on('click', function() {
	var size	= parseInt(jQuery('html').css('font-size')) - 1;
	jQuery('html').css("font-size", size + "px");
});
jQuery('#font-reset').on('click', function() {
	jQuery('html').css("font-size", "");
});
<?php endif; ?>
</script>
<?php echo $this->params->get('customScriptEnd',''); ?>