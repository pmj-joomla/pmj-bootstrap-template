<?php
defined('_JEXEC') or die;
?>
<?php if ($positionHeaderLeftCount || $positionHeaderRightCount || $logo || $this->params->get('display_sitename',1)) : ?>
<!-- Header -->
<header class="<?php echo $this->params->get('static','static') === 'static' && $this->params->get('headerBreakout',0) ? 'breakout ' : 'row ' ; ?>header" role="banner">
	<?php echo $this->params->get('static','static') === 'static' && $this->params->get('headerBreakout',0) ? '<div class="container"><div class="row">' : '' ; ?>
	<?php if ($positionHeaderLeftCount) : ?>
	<div class="col-lg-4 d-none d-lg-block text-left">
		<jdoc:include type="modules" name="header-left" style="xhtml" />
	</div>
	<?php endif; ?>
	<div class="<?php echo $class; ?>">
		<div class="">
			<?php if ($positionHeaderCenterCount) : ?>
			<jdoc:include type="modules" name="header-center" style="xhtml" />
			<?php elseif ($logo) : ?>
			<a href="/"><img srcset="
				<?php echo $logo; ?> 300w,
				<?php echo $logo; ?> 460w,
				<?php echo $logo; ?> 640w,
				<?php echo $logo; ?> 800w,
				"
				sizes="
				(max-width:414px) 100vw,
				(max-width:576px) 55vw,
				(max-width:768px) 45vw,
				(max-width:992px) 35vw,
				(max-width:1200px) 35vh,
				35vh
				"
				alt="<?php echo $this->sitename; ?>"
				title="<?php echo $this->sitename; ?>"
				class="img-fluid"
			/></a>
			<?php elseif ($this->params->get('headerDisplaySitename',1)) : ?>
			<h1><?php echo $this->params->get('headerLinkSitename',1) ? '<a href="/" rel="nofollow" title="'.$this->sitename.'">'.$this->sitename.'</a>' : $this->sitename; ?></h1>
			<?php endif ; ?>
		</div>
	</div>
	<?php if ($positionHeaderLeftCount) : ?>
	<div class="col-sm-6 d-lg-none text-center text-sm-left">
		<jdoc:include type="modules" name="header-left" style="xhtml" />
	</div>
	<?php endif; ?>
	<?php if ($positionHeaderRightCount) : ?>
	<div class="col-sm-6 col-lg-4 text-left text-sm-right">
		<jdoc:include type="modules" name="header-right" style="xhtml" />
	</div>
	<?php endif; ?>
	<?php echo $this->params->get('static','static') === 'static' && $this->params->get('headerBreakout',0) ? '</div></div>' : '' ; ?>
</header>
<?php endif; ?>