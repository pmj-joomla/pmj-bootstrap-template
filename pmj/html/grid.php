<?php
defined('_JEXEC') or die;
?>
<!-- rows -->
<?php foreach ($grid as $row_id => $row) : ?>
	<?php $class	= $row['class'] ? ' '.$row['class'] : ''; ?>
	<div id="<?php echo $row_id; ?>" class="<?php echo $this->params->get('static','static') === 'static' && $row['breakout'] ? 'breakout' : 'row' ; ?><?php echo $class; ?>">
	<?php echo $this->params->get('static','static') === 'static' && $row['breakout'] ? '<div class="container"><div class="row">' : '' ; ?>
		
		<!-- columns -->
		<?php foreach ($row['columns'] as $column_id => $column) : ?>
			<div id="<?php echo $column_id; ?>"<?php echo $column['class'] ? ' class="'.$column['class'].'"' : ''; ?>>
				
				<!-- positions -->
				<?php if (is_array($column['positions']) || is_object($column['positions'])) : ?>
				<?php foreach ($column['positions'] as $position) : ?>
					<?php if ($position->position === 'component') : ?>
						<div class="content">
							<jdoc:include type="message" />
							<jdoc:include type="component" />
						</div>
					<?php else : ?>
						<?php $module	= '<jdoc:include type="modules" name="'.$position->position.'" style="'.$position->chrome.'" ';
						if ($position->chrome === 'card' || $position->chrome === 'hcard')
						{
							if ($position->card_border_color)
							{
								$module	.= 'card_border_color="'.$position->card_border_color.'" ';
							}
							
							if ($position->card_text_color)
							{
								$module	.= 'card_text_color="'.$position->card_text_color.'" ';
							}
							
							if ($position->card_bg_color)
							{
								$module	.= 'card_bg_color="'.$position->card_bg_color.'" ';
							}
							
							if ($position->card_header_bg_color)
							{
								$module	.= 'card_header_bg_color="'.$position->card_header_bg_color.'" ';
							}
							
							if ($position->card_header_text_color)
							{
								$module	.= 'card_header_text_color="'.$position->card_header_text_color.'" ';
							}
						}
						$module	.= '/>'; ?>
						<?php echo $module; ?>
					<?php endif; ?>
				<?php endforeach; ?>
				<?php endif; ?>
				
			</div>
		<?php endforeach; ?>
		
	<?php echo $this->params->get('static','static') === 'static' && $row['breakout'] ? '</div></div>' : '' ; ?>
	</div>
<?php endforeach; ?>