<?php
defined('_JEXEC') or die;

class PMJBootstrap
{
	/**
	* variable $template
	* Joomla\CMS\Document\HtmlDocument object for this template
	* @private
	* @var object
	*/
	private $template;
	
	/**
	* variable $templatePath
	* The current template path
	* @public
	* @var string
	*/
	private $templatePath;
	
	/**
	* variable $fontsPath
	* The path to custom fonts
	* @public
	* @var string
	*/
	private $fontsPath;
	
	/**
	* variable $params
	* Joomla\Registry\Registry object for this template
	* @private
	* @var object
	*/
	private $params;
	
	/**
	* variable $styleID
	* The current style id
	* @public
	* @var string
	*/
	private $styleID;
	
	public function __construct(Joomla\CMS\Document\HtmlDocument $template,$styleID)
	{
		// set template object
		$this->template	= $template;
		// set template path
		$this->templatePath	= JPATH_THEMES.'/'.$template->template.'/';
		// set cutom fonts path
		$this->fontsPath	= $this->templatePath.'fonts/custom/';
		// set params object
		$this->params	= $template->params;
		// set style ID
		$this->styleID	= $styleID;
	}
	
	public function compileBootstrap()
	{
		$custom_scss_file	= $this->scssSource();
		$custom_css_file	= $this->templatePath.'css/pmj-style'.$this->styleID.'.min.css';
		
		if ($custom_scss_file)
		{
			$customStylesPath	= $this->templatePath . 'pmj/scss/';
			$bootstrapPath	= $this->templatePath . 'scss/bootstrap/';
			
			// load scssphp
			JLoader::register('ScssPhp\ScssPhp\Compiler',$this->templatePath.'libs/scssphp/scss.inc.php');
			$scss = new ScssPhp\ScssPhp\Compiler();
			$scss->setImportPaths(array($customStylesPath, $bootstrapPath));
			$scss->setFormatter(new ScssPhp\ScssPhp\Formatter\Crunched());
			
			$bootstrapScss = $scss->compile(file_get_contents($custom_scss_file));
			file_put_contents($custom_css_file, $bootstrapScss);
		}
	}
	
	private function scssSource()
	{
		$params	= $this->params;
		$spacers	= $this->joomla2php($this->params->get('spacers','{ "spacers_id":["0","1","2","3","4","5"], "spacers_value":["0","($spacer * .25)","($spacer * .5)","$spacer","($spacer * 1.5)","($spacer * 3)"] }'));
		$CustomFileText	= $this->joomla2php($this->params->get('custom-file-text','{ "lang":["en","de"], "text":["Browse","Durchsuchen"] }'));
		$CustomColors	= $this->params->get('customColors',json_decode('{
			"customColors0":{"custom_color_name":"custom1","custom_color_value":"#abcdef"},
			"customColors1":{"custom_color_name":"custom2","custom_color_value":"#fedcba"},
			"customColors2":{"custom_color_name":"custom3","custom_color_value":"#012345"},
			"customColors3":{"custom_color_name":"custom4","custom_color_value":"#543210"}
		}'));
		$custom_scss_file	= $this->templatePath.'pmj/scss/pmj-style'.$this->styleID.'.scss';
		
		$scssSource	= '';
		
		// fonts
		$fonts	= array();
		if ($this->params->get('fontheading','zilla-slab-highlight-v5-latin/zilla-slab-highlight-v5-latin-regular') || $this->params->get('fontbase','zilla-slab-v3-latin/zilla-slab-v3-latin-regular'))
		{
			// load fontInfo class
			JLoader::register('PMJFont',$this->templatePath.'pmj/libs/font.php');
			$PMJFont	= new PMJFont;
			
			// fontheading
			if ($this->params->get('fontheading','zilla-slab-highlight-v5-latin/zilla-slab-highlight-v5-latin-regular'))
			{
				$fonts['fontheading']['file']	= $this->params->get('fontheading','zilla-slab-highlight-v5-latin/zilla-slab-highlight-v5-latin-regular');
				$fonts['fontheading']['name']	= $PMJFont->get_friendly_ttf_name($this->fontsPath.$fonts['fontheading']['file'].'.ttf')['fontfamily'];
			}
			
			// fontbase
			if ($this->params->get('fontbase','zilla-slab-v3-latin/zilla-slab-v3-latin-regular'))
			{
				$fonts['fontbase']['file']	= $this->params->get('fontbase','zilla-slab-v3-latin/zilla-slab-v3-latin-regular');
				$fonts['fontbase']['name']	= $PMJFont->get_friendly_ttf_name($this->fontsPath.$fonts['fontbase']['file'].'.ttf')['fontfamily'];
			}
		}
		
		// load variables
		require_once($this->templatePath.'pmj/libs/bootstrap_variables.php');
		
		// add navbar padding
		if ($this->params->get('navbarPosition',0) === 'top' && $this->params->get('enablePMJ',1))
		{
			$scssSource	.= '
			body {
				padding-top: ($nav-link-height + $navbar-padding-y * 2);
			}
			'.PHP_EOL;
		}
		
		// add bootstrap
		$scssSource	.= '@import "bootstrap";'.PHP_EOL;
		// add gradient mixin
		$scssSource	.= '@import "../../pmj/scss/mixins/gradients";'.PHP_EOL;
		
		// add breakout
		if ($this->params->get('static','static') === 'static' && $this->params->get('enablePMJ',1))
		{
			//$scssSource	.= 'html { width: calc(100% + calc(100vw - 100%)); overflow-x: hidden; }'.PHP_EOL;
			$scssSource	.= 'html { overflow-y: scroll; overflow-x: hidden; }'.PHP_EOL;
			$scssSource	.= '.breakout { width: 100vw; position: relative; left: calc(-1 * (100vw - 100%) / 2); }'.PHP_EOL;
			if ($this->params->get('navbarTopWidth','fullcontainer') !== 'full' || (($params->get('navbarStickyonscroll',0) && $params->get('navbarPosition',0) === 'before') || ($params->get('navbarStickyonscroll',0) && $params->get('navbarPosition',0) === 'after')))
			{
				//$scssSource	.= '.fixed-top { left: calc(100vw - 100%); }'.PHP_EOL;
			}
		}
		
		// layout height
		if ($this->params->get('height','') === 'screen' && $this->params->get('enablePMJ',1))
		{
			if ($this->params->get('navbarPosition',0) === 'top')
			{
				$scssSource	.= '.frame { min-height: calc(100vh - (#{$nav-link-height} + #{$navbar-padding-y} * 2)); }'.PHP_EOL;
			}
			else
			{
				$scssSource	.= '.frame { min-height: 100vh; }'.PHP_EOL;
			}
		}
		
		// add font resizer
		if ($this->params->get('enableFontResizer',0) && $this->params->get('enablePMJ',1))
		{
			$scssSource	.= '#sizer { position: fixed; top: 0; right: 0; left: 0;'.($this->params->get('navbarPosition',0) === 'top' ? ' padding-top: ($nav-link-height + $navbar-padding-y * 2);' : '').' }'.PHP_EOL;
		}
		
		// add font face
		$file	= '';
		foreach ($fonts as $font)
		{
			if ($file !== $font['file'])
			{
				$file	= $font['file'];
				$scssSource	.= $this->fontsCss($font['name'],$font['file']).PHP_EOL;
			}
		}
		
		// selected text background
		if ($this->params->get('fontSelection','primary'))
		{
			$scssSource	.= '::selection {'.PHP_EOL;
			$scssSource	.= 'background-color: $'.$this->params->get('fontSelection','primary').';'.PHP_EOL;
			$scssSource	.= 'color: color-yiq($'.$this->params->get('fontSelection','primary').');'.PHP_EOL;
			$scssSource	.= '}'.PHP_EOL;
		}
		
		// CUSTOM (S)CSS
		// load custom (s)css files
		if ($this->params->get('customCssFiles',''))
		{
			$cssfiles	= $this->params->get('customCssFiles','');
			// remove spaces
			$cssfiles	= explode(',',str_replace(' ','',$cssfiles));
			if (is_array($cssfiles))
			{
				foreach ($cssfiles as $cssfile)
				{
					// don't parse external
					if (strpos($cssfile,'//') === false)
					{
						$scssSource	.= file_get_contents($this->templatePath.'css/'.$cssfile).PHP_EOL;
					}
				}
			}
		}
		// .frame
		if ($this->params->get('customFrame','') && $this->params->get('enablePMJ',1))
		{
			$scssSource	.= '.frame {'.PHP_EOL;
			$scssSource	.= $this->params->get('customFrame','').PHP_EOL;
			$scssSource	.=	'}'.PHP_EOL;
		}
		// #navbar
		if ($this->params->get('customNavbar','') && $this->params->get('enablePMJ',1))
		{
			$scssSource	.= '.navbar {'.PHP_EOL;
			$scssSource	.= $this->params->get('customNavbar','').PHP_EOL;
			$scssSource	.=	'}'.PHP_EOL;
		}
		// .header
		if ($this->params->get('customHeader','') && $this->params->get('enablePMJ',1))
		{
			$scssSource	.= '.header {'.PHP_EOL;
			$scssSource	.= $this->params->get('customHeader','').PHP_EOL;
			$scssSource	.=	'}'.PHP_EOL;
		}
		// .content
		if ($this->params->get('customContent','') && $this->params->get('enablePMJ',1))
		{
			$scssSource	.= '.content {'.PHP_EOL;
			$scssSource	.= $this->params->get('customContent','').PHP_EOL;
			$scssSource	.=	'}'.PHP_EOL;
		}
		// .footer
		if ($this->params->get('customFooter','') && $this->params->get('enablePMJ',1))
		{
			$scssSource	.= '.footer {'.PHP_EOL;
			$scssSource	.= $this->params->get('customFooter','').PHP_EOL;
			$scssSource	.=	'}'.PHP_EOL;
		}
		// custom style
		if ($this->params->get('customStyle',''))
		{
			$scssSource	.= $this->params->get('customStyle','').PHP_EOL;
		}
		
		// check if something has changed
		$current_file	= '';
		if (file_exists($custom_scss_file))
		{
			$current_file	= file_get_contents($custom_scss_file);
		}
		
		// write new content to custom_scss_file
		if (!file_exists($custom_scss_file) || ($current_file && $current_file !== $scssSource))
		{
			file_put_contents($custom_scss_file, $scssSource);
			return $custom_scss_file;
		}
		else
		{
			return null;
		}
	}
	
	private function fontsCss($fontName,$fontFile)
	{
		$return	= "@font-face
{
	font-family: '".$fontName."';
	font-style: normal;
	font-weight: 400;
	src: url('../fonts/custom/".$fontFile.".eot');
	src: local('".$fontName." Regular'), local('".str_replace(' ','',$fontName)."-Regular'),
		url('../fonts/custom/".$fontFile.".eot?#iefix') format('embedded-opentype'),
		url('../fonts/custom/".$fontFile.".woff2') format('woff2'),
		url('../fonts/custom/".$fontFile.".woff') format('woff'),
		url('../fonts/custom/".$fontFile.".ttf') format('truetype'),
		url('../fonts/custom/".$fontFile.".svg#".str_replace(' ','',$fontName)."') format('svg');
}
		";
		
		return $return;
	}
	
	private function joomla2php($inputs=array())
	{
		$inputs	= json_decode($inputs,true);
		$output	= array();
		foreach ($inputs as $index => $input)
		{
			foreach ($input as $i => $value)
			{
				$output[$i][$index]	= $value;
			}
		}
		return $output;
	}
}