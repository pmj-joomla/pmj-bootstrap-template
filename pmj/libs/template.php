<?php
defined('_JEXEC') or die;

class PMJTemplate
{
	/**
	* variable $template
	* Joomla\CMS\Document\HtmlDocument object for this template
	* @private
	* @var object
	*/
	private $template;
	
	/**
	* variable $app
	* JFactory Application
	* @private
	* @var object
	*/
	private $app;
	
	/**
	* variable $doc
	* JFactory Document
	* @private
	* @var object
	*/
	private $doc;
	
	/**
	* variable $user
	* JFactory User
	* @private
	* @var object
	*/
	public $user;
	
	/**
	* variable $doc
	* JFactory Document
	* @private
	* @var object
	*/
	private $menu;
	
	/**
	* variable $config
	* JFactory Config
	* @private
	* @var object
	*/
	private $config;
	
	/**
	* variable $params
	* Joomla\CMS\Document\HtmlDocument Parameter object for this template
	* @private
	* @var object
	*/
	private $params;
	
	/**
	* variable $sitename
	* The sitename
	* @public
	* @var string
	*/
	public $sitename;
	
	/**
	* variable $templatePath
	* The current template path
	* @public
	* @var string
	*/
	public $templatePath;
	
	/**
	* variable $styleID
	* The current style id
	* @public
	* @var string
	*/
	public $styleID;
	
	public function __construct(Joomla\CMS\Document\HtmlDocument $template)
	{
		// set template object
		$this->template	= $template;
		// load application
		$this->app	= JFactory::getApplication();
		// load document
		$this->doc	= JFactory::getDocument();
		// load document
		$this->user	= JFactory::getUser();
		// load menu
		$this->menu	= $this->app->getMenu();
		// set config
		$this->config	= JFactory::getConfig();
		// set params
		$this->params	= $template->params;
		// set sitename
		$this->sitename	= htmlspecialchars($this->app->get('sitename'), ENT_QUOTES, 'UTF-8');
		// set template path
		$this->templatePath	= JPATH_THEMES.'/'.$template->template.'/';
		// set style ID
		$this->styleID	= $this->app->getTemplate('template')->id;
		
		// load PMJBootstrap class
		JLoader::register('PMJBootstrap',$this->templatePath.'pmj/libs/bootstrap.php');
		
		// remove frontend editing
		$this->config->set('frontediting','0');
	}
	
	public function renderTemplate()
	{
		$start	= microtime(true);
		// language override
		$this->langOver();
		$render	= '';
		// run bootstrap compiler
		$bootstrap	= new PMJBootstrap($this->template,$this->styleID);
		$bootstrap->compileBootstrap();
		JHtml::_('stylesheet', 'pmj-style'.$this->styleID.'.min.css', array('version' => 'auto', 'relative' => true));
		
		// navbar top/before
		if ($this->params->get('navbarPosition',0) === 'top' || $this->params->get('navbarPosition',0) === 'before')
		{
			$render	.= $this->renderNavbar();
		}
		// header
		if ($this->params->get('enableHeader',1))
		{
			$render	.= $this->renderHeader();
		}
		
		// navbar after
		if ($this->params->get('navbarPosition',0) === 'after')
		{
			$render	.= $this->renderNavbar();
		}
		
		// grid
		$render .= $this->renderGrid();
		
		// footer
		if ($this->params->get('enableFooter',1))
		{
			$render .= $this->renderFooter();
		}
		//echo 'Executed in: '. (microtime(true) - $start) . ' s';
		
		// output
		return $render;
	}
	
	private function renderGrid()
	{
		JLoader::register('PMJGrid',$this->templatePath.'pmj/libs/grid.php');
		
		$gridRows	= $this->params->get('gridRows','{
			"gridRows0":{"row_id":"topRow",row_size:"container","row_class":"justify-content-center"},
			"gridRows1":{"row_id":"contentRow",row_size:"container","row_class":""},
			"gridRows2":{"row_id":"bottomRow",row_size:"container","row_class":""}
		}');
		$gridRows	= is_object($gridRows) ? $gridRows : json_decode($gridRows);
		
		$gridColumns	= $this->params->get('gridColumns','{
			"gridColumns0":{"row_id":"topRow","column_id":"topColumn","column_width_xs":"","column_width_sm":"","column_width_md":"","column_width_lg":"","column_width_xl":"","column_class":""},
			"gridColumns1":{"row_id":"contentRow","column_id":"leftColumn","column_width_xs":"","column_width_sm":"","column_width_md":"4","column_width_lg":"3","column_width_xl":"","column_class":""},
			"gridColumns2":{"row_id":"contentRow","column_id":"contentColumn","column_width_xs":"","column_width_sm":"col","column_width_md":"4","column_width_lg":"6","column_width_xl":"","column_class":""},
			"gridColumns3":{"row_id":"contentRow","column_id":"rightColumn","column_width_xs":"col","column_width_sm":"","column_width_md":"4","column_width_lg":"3","column_width_xl":"","column_class":""},
			"gridColumns4":{"row_id":"bottomRow","column_id":"bottomColumn","column_width_xs":"","column_width_sm":"","column_width_md":"","column_width_lg":"","column_width_xl":"","column_class":""}
		}');
		$gridColumns	= is_object($gridColumns) ? $gridColumns : json_decode($gridColumns);
		
		$gridPositions	= $this->params->get('gridPositions','{
			"gridPositions0":{"column_id":"contentColumn","position":"position-3","chrome":"xhtml","card_border_color":"","card_text_color":"","card_bg_color":"","card_header_bg_color":"","card_header_text_color":""},
			"gridPositions1":{"column_id":"contentColumn","position":"position-2","chrome":"none","card_border_color":"","card_text_color":"","card_bg_color":"","card_header_bg_color":"","card_header_text_color":""},
			"gridPositions2":{"column_id":"contentColumn","position":"component","chrome":"none","card_border_color":"","card_text_color":"","card_bg_color":"","card_header_bg_color":"","card_header_text_color":""},
			"gridPositions3":{"column_id":"rightColumn","position":"position-7","chrome":"card","card_border_color":"","card_text_color":"","card_bg_color":"","card_header_bg_color":"","card_header_text_color":""},
			"gridPositions4":{"column_id":"rightColumn","position":"position-8","chrome":"none","card_border_color":"","card_text_color":"","card_bg_color":"","card_header_bg_color":"","card_header_text_color":""},
			"gridPositions5":{"column_id":"leftColumn","position":"position-5","chrome":"none","card_border_color":"","card_text_color":"","card_bg_color":"","card_header_bg_color":"","card_header_text_color":""}
		}');
		$gridPositions	= is_object($gridPositions) ? $gridPositions : json_decode($gridPositions);
		
		$grid	= new PMJGrid($gridRows,$gridColumns,$gridPositions,$this->template);
		
		return $grid->getGrid();
	}
	
	private function renderHeader()
	{
		// header layout
		$class	= '';
		$positionHeaderCenterCount = $this->template->countModules('header-center');
		$positionHeaderLeftCount = $this->template->countModules('header-left');
		$positionHeaderRightCount = $this->template->countModules('header-right');
		if ($positionHeaderLeftCount && $positionHeaderRightCount)
		{
			$class	= 'col-sm-12 col-lg-4';
		}
		elseif (($positionHeaderLeftCount || $positionHeaderRightCount) && !($positionHeaderLeftCount && $positionHeaderRightCount))
		{
			$class	= 'col-sm-12 col-lg-8';
		}
		else
		{
			$class	= 'col';
		}
		
		// logo
		$logo	= $this->params->get('logo','');
		if ($logo)
		{
			$logo	= JUri::root().$this->params->get('logo','');
		}
		$render	= require_once(JPATH_THEMES.'/'.$this->template->template.'/pmj/html/header.php');
		return $render;
	}
	
	private function renderNavbar()
	{
		$params	= $this->params;
		$render	= require_once($this->templatePath.'pmj/html/navbar.php');
		return $render;
	}
	
	private function renderFooter()
	{
		$footerText	= '';
		$classText	= '';
		$classBacklink	= '';
		if ($this->params->get('footerTextEnable',1))
		{
			$variables = array(
				'year'	=> date('Y'),
				'sitename'	=> $this->sitename,
			);
			$footerText = $this->params->get('footerText','&copy {year} {sitename}');

			foreach($variables as $key => $value){
				$footerText = str_replace('{'.$key.'}', $value, $footerText);
			}
		}
		
		if ($this->params->get('footerTextEnable',1))
		{
			$classText	= 'col-lg-10 text-lg-left';
			$classBacklink	= 'col-lg-2 text-lg-right';
		}
		elseif ($this->params->get('footerBacklink',1))
		{
			$classBacklink	= 'col-lg text-lg-right';
		}
		$render	= require_once(JPATH_THEMES.'/'.$this->template->template.'/pmj/html/footer.php');
		return $render;
	}
	
	private function joomla2php($inputs=array())
	{
		$inputs	= json_decode($inputs,true);
		$output	= array();
		foreach ($inputs as $index => $input)
		{
			foreach ($input as $i => $value)
			{
				$output[$i][$index]	= $value;
			}
		}
		return $output;
	}
	
	private function langOver()
	{
		// set variables
		$language	=& JFactory::getLanguage();
		$langTag	= $language->getTag();
		$overrideFile	= JPATH_ROOT.'/language/overrides/'.$langTag.'.override.ini';
		$tempFile	= $this->templatePath.'pmj/language/'.$langTag.'.pmj.ini';
		$delimiter	= ';PMJBOOTSTRAPTEMPLATEOVER'.PHP_EOL;
		
		// do we actually need to load custom language?
		if ($this->params->get('customLanguage',''))
		{
			// remove spaces
			$customFiles	= array(str_replace(' ','',$this->params->get('customLanguage','')));
			// check if there are multiple files to load
			if (strpos($customFiles[0],',') !== false)
			{
				$customFiles	= explode(',',$customFiles[0]);
			}
			if (is_array($customFiles))
			{
				// get the contents
				$customContent	= '';
				foreach ($customFiles as $customFile)
				{
					$file	= $this->templatePath.'language/'.$langTag.'/'.$langTag.'.'.$customFile.'.ini';
					if (file_exists($file))
					{
						$customContent	.= file_get_contents($file).PHP_EOL;
					}
				}
				// check if there's actually anything to compile
				if (!empty($customContent))
				{
					// check if anything has changed
					if (!file_exists($tempFile) || (file_exists($tempFile) && file_get_contents($tempFile) !== $customContent))
					{
						file_put_contents($tempFile,$customContent);
						// check if an override already exists
						if (file_exists($overrideFile))
						{
							$overrideFileContents	= explode($delimiter,file_get_contents($overrideFile));
							$compiledContents	= $overrideFileContents[0];
							$compiledContents	.= $delimiter.PHP_EOL;
							$compiledContents	.= $customContent;
							file_put_contents($overrideFile,$compiledContents);
						}
						else
						{
							$compiledContents	= $delimiter.PHP_EOL;
							$compiledContents	.= $customContent;
							file_put_contents($overrideFile,$compiledContents);
						}
						echo '<script type="text/javascript">location.reload(true);</script>';
					}
				}
			}
		}
		elseif (file_exists($tempFile))
		{
			// remove template overrides from file or delete it if it's empty
			$overrideFileContents	= explode($delimiter,file_get_contents($overrideFile));
			if (!empty($overrideFileContents[0]))
			{
				file_put_contents($overrideFile,$overrideFileContents[0]);
			}
			else
			{
				unlink($overrideFile);
			}
			
			// delete tempFile
			unlink($tempFile);
			echo '<script type="text/javascript">location.reload(true);</script>';
		}
	}
}
