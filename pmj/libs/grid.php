<?php
defined('_JEXEC') or die;

class PMJGrid
{
	public function __construct(object $gridRows,object $gridColumns,object $gridPositions,Joomla\CMS\Document\HtmlDocument $template)
	{
		$this->doc	= JFactory::getDocument();
		$this->template	= $template;
		$this->params	= $template->params;
		$this->gridRows	= $gridRows;
		$this->gridColumns	= $gridColumns;
		$this->gridPositions	= $gridPositions;
	}
	
	private function compileGrid()
	{
		$grid	= array();
		$index_rows	= 'gridRows';
		$index_columns	= 'gridColumns';
		$index_positions	= 'gridPositions';
		
		// rows
		foreach ($this->gridRows as $gr => $gridRow)
		{
			// reset widths for columns in this row
			$width_xs	= 0;
			$width_sm	= 0;
			$width_md	= 0;
			$width_lg	= 0;
			$width_xl	= 0;
			// columns
			$gridColumns	= array();
			foreach ($this->gridColumns as $gc => $gridColumn)
			{
				// continue if column is not in row
				if ($gridRow->row_id !== $gridColumn->row_id) continue;
				$gridColumns[$gc]	= $gridColumn;
			}
			if (!empty($gridColumns))
			{
				foreach ($gridColumns as $gc => $gridColumn)
				{
					// positions
					$gridPositions	= array();
					foreach ($this->gridPositions as $gp => $gridPosition)
					{
						// continue if positions is not in column
						if ($gridColumn->column_id !== $gridPosition->column_id) continue;
						if ($this->template->countModules($gridPosition->position) || $gridPosition->position === 'component')
						{
							$gridPositions[]	= $gridPosition;
						}
					}
					if (!empty($gridPositions))
					{
						// remember last shown column
						$last_visible_column	= $gridColumn->column_id;
						// remember last visible column_class
						$last_visible_column_class	= $gridColumn->column_class;
						// remember last shown width
						$last_visible_width	= array();
						
						$grid[$gridRow->row_id]['columns'][$gridColumn->column_id]['positions']	= $gridPositions;
						// compile classes
						$class	= '';
						if ($gridColumn->column_width_xs)
						{
							$last_visible_width['xs']	= $width_xs + $gridColumn->column_width_xs;
							$class	.= ' col'.(!is_numeric($gridColumn->column_width_xs) ? '' : '-'.$last_visible_width['xs']);
							$width_xs	= 0;
						}
						if ($gridColumn->column_width_sm)
						{
							$last_visible_width['sm']	= $width_sm + $gridColumn->column_width_sm;
							$class	.= ' col'.(!is_numeric($gridColumn->column_width_sm) ? '-sm' : '-sm-'.$last_visible_width['sm']);
							$width_sm	= 0;
						}
						if ($gridColumn->column_width_md)
						{
							$last_visible_width['md']	= $width_md + $gridColumn->column_width_md;
							$class	.= ' col'.(!is_numeric($gridColumn->column_width_md) ? '-md' : '-md-'.$last_visible_width['md']);
							$width_md	= 0;
						}
						if ($gridColumn->column_width_lg)
						{
							$last_visible_width['lg']	= $width_lg + $gridColumn->column_width_lg;
							$class	.= ' col'.(!is_numeric($gridColumn->column_width_lg) ? '-lg' : '-lg-'.$last_visible_width['lg']);
							$width_lg	= 0;
						}
						if ($gridColumn->column_width_xl)
						{
							$last_visible_width['xl']	= $width_xl + $gridColumn->column_width_xl;
							$class	.= ' col'.(!is_numeric($gridColumn->column_width_xl) ? '-xl' : '-xl-'.$last_visible_width['xl']);
							$width_xl	= 0;
						}
						if (empty($class))
						{
							$class	= 'col';
						}
						$grid[$gridRow->row_id]['columns'][$gridColumn->column_id]['class']	= trim($class).($gridColumn->column_class ? ' '.$gridColumn->column_class : '');
					}
					else
					{
						$class	= '';
						if ($gridColumn->column_width_xs)
						{
							if ($gridColumn === end($gridColumns))
							{
								$class	.= ' col'.(!is_numeric($gridColumn->column_width_xs) ? '' : '-'.($width_xs + $gridColumn->column_width_xs + $last_visible_width['xs']));
							}
							else
							{
								$width_xs	= $width_xs + $gridColumn->column_width_xs;
							}
						}
						if ($gridColumn->column_width_sm)
						{
							if ($gridColumn === end($gridColumns))
							{
								$class	.= ' col'.(!is_numeric($gridColumn->column_width_sm) ? '-sm' : '-sm-'.($width_sm + $gridColumn->column_width_sm + $last_visible_width['sm']));
							}
							else
							{
								$width_sm	= $width_sm + $gridColumn->column_width_sm;
							}
						}
						if ($gridColumn->column_width_md)
						{
							if ($gridColumn === end($gridColumns))
							{
								$class	.= ' col'.(!is_numeric($gridColumn->column_width_md) ? '-md' : '-md-'.($width_md + $gridColumn->column_width_md + $last_visible_width['md']));
							}
							else
							{
								$width_md	= $width_md + $gridColumn->column_width_md;
							}
						}
						if ($gridColumn->column_width_lg)
						{
							if ($gridColumn === end($gridColumns))
							{
								$class	.= ' col'.(!is_numeric($gridColumn->column_width_lg) ? '-lg' : '-lg-'.($width_lg + $gridColumn->column_width_lg + $last_visible_width['lg']));
							}
							else
							{
								$width_lg	= $width_lg + $gridColumn->column_width_lg;
							}
						}
						if ($gridColumn->column_width_xl)
						{
							if ($gridColumn === end($gridColumns))
							{
								$class	.= ' col'.(!is_numeric($gridColumn->column_width_xl) ? '-xl' : '-xl-'.($width_xl + $gridColumn->column_width_xl + $last_visible_width['xl']));
							}
							else
							{
								$width_xl	= $width_xl + $gridColumn->column_width_xl;
							}
						}
						if (!empty($class) && $gridColumn === end($gridColumns))
						{
							$grid[$gridRow->row_id]['columns'][$last_visible_column]['class']	= trim($class).($last_visible_column_class ? ' '.$last_visible_column_class : '');
						}
					}
				}
				// add row class and row size at the end to ommit the rendering of empty rows
				if (!empty($grid[$gridRow->row_id]))
				{
					// row width
					$grid[$gridRow->row_id]['breakout']	= $gridRow->row_breakout;
					// row class
					$grid[$gridRow->row_id]['class']	= $gridRow->row_class;
				}
			}
		}
		return $grid;
	}
	
	public function getGrid()
	{
		$grid	= $this->compileGrid();
		$render	= require_once(JPATH_THEMES.'/'.$this->template->template.'/pmj/html/grid.php');
		return $render;
	}
}