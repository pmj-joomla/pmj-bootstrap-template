<?php
defined('_JEXEC') or die;
//'.$params->get('spacer','1rem').'
$scssSource	= '@import "functions";
// Variables
//
// Variables should follow the `$component-state-property-size` formula for
// consistent naming. Ex: $nav-link-disabled-color and $modal-content-box-shadow-xs.

// Color system

$white:    #fff !default;
$gray-100: #f8f9fa !default;
$gray-200: #e9ecef !default;
$gray-300: #dee2e6 !default;
$gray-400: #ced4da !default;
$gray-500: #adb5bd !default;
$gray-600: #6c757d !default;
$gray-700: #495057 !default;
$gray-800: #343a40 !default;
$gray-900: #212529 !default;
$black:    #000 !default;

$grays: () !default;
$grays: map-merge(
  (
    "100": $gray-100,
    "200": $gray-200,
    "300": $gray-300,
    "400": $gray-400,
    "500": $gray-500,
    "600": $gray-600,
    "700": $gray-700,
    "800": $gray-800,
    "900": $gray-900
  ),
  $grays
);

$blue:    '.$params->get('blue','#007bff').' !default;
$indigo:  '.$params->get('indigo','#6610f2').' !default;
$purple:  '.$params->get('purple','#6f42c1').' !default;
$pink:    '.$params->get('pink','#e83e8c').' !default;
$red:     '.$params->get('red','#dc3545').' !default;
$orange:  '.$params->get('orange','#fd7e14').' !default;
$yellow:  '.$params->get('yellow','#ffc107').' !default;
$green:   '.$params->get('green','#28a745').' !default;
$teal:    '.$params->get('teal','#20c997').' !default;
$cyan:    '.$params->get('cyan','#17a2b8').' !default;'.PHP_EOL;
$lastCustomColors = end($CustomColors);
foreach ($CustomColors as $value)
{
	$scssSource	.= '$'.$value->custom_color_name.': '.$value->custom_color_value.' !default;'.PHP_EOL;
}
$scssSource	.= '
$colors: () !default;
$colors: map-merge(
  (
    "blue":       $blue,
    "indigo":     $indigo,
    "purple":     $purple,
    "pink":       $pink,
    "red":        $red,
    "orange":     $orange,
    "yellow":     $yellow,
    "green":      $green,
    "teal":       $teal,
    "cyan":       $cyan,
    "white":      $white,
    "gray":       $gray-600,
    "gray-dark":  $gray-800
  ),
  $colors
);

$primary:       '.$params->get('primary','$orange').' !default;
$secondary:     '.$params->get('secondary','$gray-600').' !default;
$success:       '.$params->get('success','$green').' !default;
$info:          '.$params->get('info','$cyan').' !default;
$warning:       '.$params->get('warning','$yellow').' !default;
$danger:        '.$params->get('danger','$red').' !default;
$light:         '.$params->get('light','$gray-100').' !default;
$dark:          '.$params->get('dark','$gray-800').' !default;

$theme-colors: () !default;
$theme-colors: map-merge(
  (
    "primary":    $primary,
    "secondary":  $secondary,
    "success":    $success,
    "info":       $info,
    "warning":    $warning,
    "danger":     $danger,
    "light":      $light,
    "dark":       $dark,'.PHP_EOL;
$lastCustomColors = end($CustomColors);
foreach ($CustomColors as $value)
{
	$scssSource	.= '    "'.$value->custom_color_name.'": $'.$value->custom_color_name.($lastCustomColors !== $value ? ',' : '').PHP_EOL;
}
$scssSource	.= '  ),
  $theme-colors
);

// Set a specific jump point for requesting color jumps
$theme-color-interval:      '.$params->get('theme-color-interval','8%').' !default;

// The yiq lightness value that determines when the lightness of color changes from "dark" to "light". Acceptable values are between 0 and 255.
$yiq-contrasted-threshold:  '.$params->get('yiq-contrasted-threshold','150').' !default;

// Customize the light and dark text colors for use in our YIQ color contrast function.
$yiq-text-dark:             '.$params->get('yiq-text-dark','$gray-900').' !default;
$yiq-text-light:            '.$params->get('yiq-text-light','$white').' !default;

// Characters which are escaped by the escape-svg function
$escaped-characters: (
  ("<", "%3c"),
  (">", "%3e"),
  ("#", "%23"),
  ("(", "%28"),
  (")", "%29"),
) !default;

// Options
//
// Quickly modify global styling by enabling or disabling optional features.

$enable-caret:                                true !default;
$enable-rounded:                              true !default;
$enable-shadows:                              false !default;
$enable-gradients:                            false !default;
$enable-transitions:                          true !default;
$enable-prefers-reduced-motion-media-query:   true !default;
$enable-hover-media-query:                    false !default; // Deprecated, no longer affects any compiled CSS
$enable-grid-classes:                         true !default;
$enable-pointer-cursor-for-buttons:           true !default;
$enable-print-styles:                         true !default;
$enable-responsive-font-sizes:                false !default;
$enable-validation-icons:                     true !default;
$enable-deprecation-messages:                 true !default;


// Spacing
//
// Control the default styling of most Bootstrap elements by modifying these
// variables. Mostly focused on spacing.
// You can add more entries to the $spacers map, should you need more variation.

$spacer: '.$params->get('spacer','1rem').' !default;
$spacers: () !default;
$spacers: map-merge(
  ('.PHP_EOL;
$lastSpacer = end($spacers);
foreach ($spacers as $value)
{
	$scssSource	.= '    '.$value['spacers_id'].': '.$value['spacers_value'].($lastSpacer !== $value ? ',' : '').PHP_EOL;
}
$scssSource	.= '  ),
  $spacers
);

// This variable affects the `.h-*` and `.w-*` classes.
$sizes: () !default;
// stylelint-disable-next-line scss/dollar-variable-default
$sizes: map-merge(
  (
    25: 25%,
    50: 50%,
    75: 75%,
    100: 100%,
    auto: auto
  ),
  $sizes
);


// Body
//
// Settings for the `<body>` element.

$body-bg:                   '.$params->get('body-bg','$white').' !default;
$body-color:                '.$params->get('body-color','$gray-900').' !default;

// Links
//
// Style anchor elements.

$link-color:                '.$params->get('link-color','theme-color("primary")').' !default;
$link-decoration:           '.$params->get('link-decoration','none').' !default;
$link-hover-color:          '.$params->get('link-hover-color','darken($link-color, 15%)').' !default;
$link-hover-decoration:     '.$params->get('link-hover-decoration','underline').' !default;
// Darken percentage for links with `.text-*` class (e.g. `.text-success`)
$emphasized-link-hover-darken-percentage: '.$params->get('emphasized-link-hover-darken-percentage','15%').' !default;

// Paragraphs
//
// Style p element.

$paragraph-margin-bottom:   '.$params->get('paragraph-margin-bottom','1rem').' !default;


// Grid breakpoints
//
// Define the minimum dimensions at which your layout will change,
// adapting to different screen sizes, for use in media queries.

$grid-breakpoints: (
  xs: '.$params->get('grid-breakpoints-xs','0').',
  sm: '.$params->get('grid-breakpoints-sm','576px').',
  md: '.$params->get('grid-breakpoints-md','768px').',
  lg: '.$params->get('grid-breakpoints-lg','992px').',
  xl: '.$params->get('grid-breakpoints-xl','1200px').'
) !default;

@include _assert-ascending($grid-breakpoints, "$grid-breakpoints");
@include _assert-starts-at-zero($grid-breakpoints, "$grid-breakpoints");


// Grid containers
//
// Define the maximum width of `.container` for different screen sizes.

$container-max-widths: (
  sm: '.$params->get('container-max-widths-sm','540px').',
  md: '.$params->get('container-max-widths-md','720px').',
  lg: '.$params->get('container-max-widths-lg','960px').',
  xl: '.$params->get('container-max-widths-xl','1140px').'
) !default;

@include _assert-ascending($container-max-widths, "$container-max-widths");


// Grid columns
//
// Set the number of columns and specify the width of the gutters.

$grid-columns:                '.$params->get('grid-columns','12').' !default;
$grid-gutter-width:           '.$params->get('grid-gutter-width','30px').' !default;
$grid-row-columns:            '.$params->get('grid-row-columns','6').' !default;

// Components
//
// Define common padding and border radius sizes and more.

$line-height-lg:              '.$params->get('line-height-lg','1.5').' !default;
$line-height-sm:              '.$params->get('line-height-sm','1.5').' !default;

$border-width:                '.$params->get('border-width','1px').' !default;
$border-color:                '.$params->get('border-color','$gray-300').' !default;

$border-radius:               '.$params->get('border-radius','.25rem').' !default;
$border-radius-lg:            '.$params->get('border-radius-lg','.3rem').' !default;
$border-radius-sm:            '.$params->get('border-radius-sm','.2rem').' !default;

$rounded-pill:                '.$params->get('rounded-pill','50rem').' !default;

$box-shadow-sm:               '.$params->get('box-shadow-sm','0 .125rem .25rem rgba($black, .075)').' !default;
$box-shadow:                  '.$params->get('box-shadow','0 .5rem 1rem rgba($black, .15)').' !default;
$box-shadow-lg:               '.$params->get('box-shadow-lg','0 1rem 3rem rgba($black, .175)').' !default;

$component-active-color:      '.$params->get('component-active-color','$white').' !default;
$component-active-bg:         '.$params->get('component-active-bg','theme-color("primary")').' !default;

$caret-width:                 '.$params->get('caret-width','.3em').' !default;
$caret-vertical-align:        '.$params->get('caret-vertical-align','$caret-width * .85').' !default;
$caret-spacing:               '.$params->get('caret-spacing','$caret-width * .85').' !default;

$transition-base:             '.$params->get('transition-base','all .2s ease-in-out').' !default;
$transition-fade:             '.$params->get('transition-fade','opacity .15s linear').' !default;
$transition-collapse:         '.$params->get('transition-collapse','height .35s ease').' !default;

$embed-responsive-aspect-ratios: () !default;
$embed-responsive-aspect-ratios: join(
  (
    (21 9),
    (16 9),
    (4 3),
    (1 1),
  ),
  $embed-responsive-aspect-ratios
);

// Typography
//
// Font, line-height, and color for body text, headings, and more.

// stylelint-disable value-keyword-case
$font-family-sans-serif:      '.($fonts && $fonts['fontbase']['name'] ? '"'.$fonts['fontbase']['name'].'"' : '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"').' !default;
$font-family-monospace:       SFMono-Regular, Menlo, Monaco, Consolas, "Liberation Mono", "Courier New", monospace !default;
$font-family-base:            $font-family-sans-serif !default;
// stylelint-enable value-keyword-case

$font-size-base:              '.$params->get('font-size-base','1rem').' !default; // Assumes the browser default, typically `16px`
$font-size-lg:                '.$params->get('font-size-lg','$font-size-base * 1.25').' !default;
$font-size-sm:                '.$params->get('font-size-sm','$font-size-base * .875').' !default;

$font-weight-lighter:         '.$params->get('font-weight-light','lighter').' !default;
$font-weight-light:           '.$params->get('font-weight-light','300').' !default;
$font-weight-normal:          '.$params->get('font-weight-normal','400').' !default;
$font-weight-bold:            '.$params->get('font-weight-bold','700').' !default;
$font-weight-bolder:          '.$params->get('font-weight-bold','bolder').' !default;

$font-weight-base:            '.$params->get('font-weight-base','$font-weight-normal').' !default;
$line-height-base:            '.$params->get('line-height-base','1.5').' !default;

$h1-font-size:                '.$params->get('h1-font-size','$font-size-base * 2.5').' !default;
$h2-font-size:                '.$params->get('h2-font-size','$font-size-base * 2').' !default;
$h3-font-size:                '.$params->get('h3-font-size','$font-size-base * 1.75').' !default;
$h4-font-size:                '.$params->get('h4-font-size','$font-size-base * 1.5').' !default;
$h5-font-size:                '.$params->get('h5-font-size','$font-size-base * 1.25').' !default;
$h6-font-size:                '.$params->get('h6-font-size','$font-size-base').' !default;

$headings-margin-bottom:      '.$params->get('headings-margin-bottom','$spacer / 2').' !default;
$headings-font-family:        '.($fonts && $fonts['fontheading']['name'] ? '"'.$fonts['fontheading']['name'].'"' : 'null').' !default;
$headings-font-weight:        '.$params->get('headings-font-weight','500').' !default;
$headings-line-height:        '.$params->get('headings-line-height','1.2').' !default;
$headings-color:              '.$params->get('headings-color','null').' !default;

$display1-size:               '.$params->get('display1-size','6rem').' !default;
$display2-size:               '.$params->get('display2-size','5.5rem').' !default;
$display3-size:               '.$params->get('display3-size','4.5rem').' !default;
$display4-size:               '.$params->get('display4-size','3.5rem').' !default;

$display1-weight:             '.$params->get('display1-weight','300').' !default;
$display2-weight:             '.$params->get('display2-weight','300').' !default;
$display3-weight:             '.$params->get('display3-weight','300').' !default;
$display4-weight:             '.$params->get('display4-weight','300').' !default;
$display-line-height:         '.$params->get('display-line-height','$headings-line-height').' !default;

$lead-font-size:              '.$params->get('lead-font-size','$font-size-base * 1.25').' !default;
$lead-font-weight:            '.$params->get('lead-font-weight','300').' !default;

$small-font-size:             '.$params->get('small-font-size','80%').' !default;

$text-muted:                  '.$params->get('text-muted','$gray-600').' !default;

$blockquote-small-color:      '.$params->get('blockquote-small-color','$gray-600').' !default;
$blockquote-small-font-size:  '.$params->get('blockquote-small-font-size','$small-font-size').' !default;
$blockquote-font-size:        '.$params->get('blockquote-font-size','$font-size-base * 1.25').' !default;

$hr-border-color:             '.$params->get('hr-border-color','rgba($black, .1)').' !default;
$hr-border-width:             '.$params->get('hr-border-width','$border-width').' !default;

$mark-padding:                '.$params->get('mark-padding','.2em').' !default;

$dt-font-weight:              '.$params->get('dt-font-weight','$font-weight-bold').' !default;

$kbd-box-shadow:              '.$params->get('kbd-box-shadow','inset 0 -.1rem 0 rgba($black, .25)').' !default;
$nested-kbd-font-weight:      '.$params->get('nested-kbd-font-weight','$font-weight-bold').' !default;

$list-inline-padding:         '.$params->get('list-inline-padding','.5rem').' !default;

$mark-bg:                     '.$params->get('mark-bg','#fcf8e3').' !default;

$hr-margin-y:                 '.$params->get('hr-margin-y','$spacer').' !default;


// Tables
//
// Customizes the `.table` component with basic values, each used across all table variations.

$table-cell-padding:          '.$params->get('table-cell-padding','.75rem').' !default;
$table-cell-padding-sm:       '.$params->get('table-cell-padding-sm','.3rem').' !default;

$table-color:                 '.$params->get('table-color','$body-color').' !default;
$table-bg:                    '.$params->get('table-bg','null').' !default;
$table-accent-bg:             '.$params->get('table-accent-bg','rgba($black, .05)').' !default;
$table-hover-color:           '.$params->get('table-hover-color','$table-color').' !default;
$table-hover-bg:              '.$params->get('table-hover-bg','rgba($black, .075)').' !default;
$table-active-bg:             '.$params->get('table-active-bg','$table-hover-bg').' !default;

$table-border-width:          '.$params->get('table-border-width','$border-width').' !default;
$table-border-color:          '.$params->get('table-border-color','$border-color').' !default;

$table-head-bg:               '.$params->get('table-head-bg','$gray-200').' !default;
$table-head-color:            '.$params->get('table-head-color','$gray-700').' !default;

$table-dark-color:            '.$params->get('table-dark-color','$white').' !default;
$table-dark-bg:               '.$params->get('table-dark-bg','$gray-900').' !default;
$table-dark-accent-bg:        '.$params->get('table-dark-accent-bg','rgba($white, .05)').' !default;
$table-dark-hover-color:      '.$params->get('table-dark-hover-color','$table-dark-color').' !default;
$table-dark-hover-bg:         '.$params->get('table-dark-hover-bg','rgba($white, .075)').' !default;
$table-dark-border-color:     '.$params->get('table-dark-border-color','lighten($table-dark-bg, 7.5%)').' !default;

$table-striped-order:         '.$params->get('table-striped-order','odd').' !default;

$table-caption-color:         '.$params->get('table-caption-color','$text-muted').' !default;


// Buttons + Forms
//
// Shared variables that are reassigned to `$input-` and `$btn-` specific variables.

$input-btn-padding-y:         '.$params->get('input-btn-padding-y','.375rem').' !default;
$input-btn-padding-x:         '.$params->get('input-btn-padding-x','.75rem').' !default;
$input-btn-font-family:       '.$params->get('input-btn-font-family','null').' !default;
$input-btn-font-size:         '.$params->get('input-btn-font-size','$font-size-base').' !default;
$input-btn-line-height:       '.$params->get('input-btn-line-height','$line-height-base').' !default;

$input-btn-focus-width:       '.$params->get('input-btn-focus-width','.2rem').' !default;
$input-btn-focus-color:       '.$params->get('input-btn-focus-color','rgba($component-active-bg, .25)').' !default;
$input-btn-focus-box-shadow:  '.$params->get('input-btn-focus-box-shadow','0 0 0 $input-btn-focus-width $input-btn-focus-color').' !default;

$input-btn-padding-y-sm:      '.$params->get('input-btn-padding-y-sm','.25rem').' !default;
$input-btn-padding-x-sm:      '.$params->get('input-btn-padding-x-sm','.5rem').' !default;
$input-btn-font-size-sm:      '.$params->get('input-btn-font-size-sm','$font-size-sm').' !default;
$input-btn-line-height-sm:    '.$params->get('input-btn-line-height-sm','$line-height-sm').' !default;

$input-btn-padding-y-lg:      '.$params->get('input-btn-padding-y-lg','.5rem').' !default;
$input-btn-padding-x-lg:      '.$params->get('input-btn-padding-x-lg','1rem').' !default;
$input-btn-font-size-lg:      '.$params->get('input-btn-font-size-lg','$font-size-lg').' !default;
$input-btn-line-height-lg:    '.$params->get('input-btn-line-height-lg','$line-height-lg').' !default;

$input-btn-border-width:      '.$params->get('input-btn-border-width','$border-width').' !default;


// Buttons
//
// For each of Bootstrap\'s buttons, define text, background, and border color.

$btn-padding-y:               '.$params->get('btn-padding-y','$input-btn-padding-y').' !default;
$btn-padding-x:               '.$params->get('btn-padding-x','$input-btn-padding-x').' !default;
$btn-font-family:             '.$params->get('btn-font-family','$input-btn-font-family').' !default;
$btn-font-size:               '.$params->get('btn-font-size','$input-btn-font-size').' !default;
$btn-line-height:             '.$params->get('btn-line-height','$input-btn-line-height').' !default;
$btn-white-space:             '.$params->get('btn-white-space','null').' !default; // Set to `nowrap` to prevent text wrapping

$btn-padding-y-sm:            '.$params->get('btn-padding-y-sm','$input-btn-padding-y-sm').' !default;
$btn-padding-x-sm:            '.$params->get('btn-padding-x-sm','$input-btn-padding-x-sm').' !default;
$btn-font-size-sm:            '.$params->get('btn-font-size-sm','$input-btn-font-size-sm').' !default;
$btn-line-height-sm:          '.$params->get('btn-line-height-sm','$input-btn-line-height-sm').' !default;

$btn-padding-y-lg:            '.$params->get('btn-padding-y-lg','input-btn-padding-y-lg').' !default;
$btn-padding-x-lg:            '.$params->get('btn-padding-x-lg','$input-btn-padding-x-lg').' !default;
$btn-font-size-lg:            '.$params->get('btn-font-size-lg','$input-btn-font-size-lg').' !default;
$btn-line-height-lg:          '.$params->get('btn-line-height-lg','$input-btn-line-height-lg').' !default;

$btn-border-width:            '.$params->get('btn-border-width','$input-btn-border-width').' !default;

$btn-font-weight:             '.$params->get('btn-font-weight','$font-weight-normal').' !default;
$btn-box-shadow:              '.$params->get('btn-box-shadow','inset 0 1px 0 rgba($white, .15), 0 1px 1px rgba($black, .075)').' !default;
$btn-focus-width:             '.$params->get('btn-focus-width','$input-btn-focus-width').' !default;
$btn-focus-box-shadow:        '.$params->get('btn-focus-box-shadow','$input-btn-focus-box-shadow').' !default;
$btn-disabled-opacity:        '.$params->get('btn-disabled-opacity','.65').' !default;
$btn-active-box-shadow:       '.$params->get('btn-active-box-shadow','inset 0 3px 5px rgba($black, .125)').' !default;

$btn-link-disabled-color:     '.$params->get('btn-link-disabled-color','$gray-600').' !default;

$btn-block-spacing-y:         '.$params->get('btn-block-spacing-y','.5rem').' !default;

// Allows for customizing button radius independently from global border radius
$btn-border-radius:           '.$params->get('btn-border-radius','$border-radius').' !default;
$btn-border-radius-lg:        '.$params->get('btn-border-radius-lg','$border-radius-lg').' !default;
$btn-border-radius-sm:        '.$params->get('btn-border-radius-sm','border-radius-sm').' !default;

$btn-transition:              '.$params->get('btn-transition','color .15s ease-in-out, background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out').' !default;


// Forms

$label-margin-bottom:                   '.$params->get('label-margin-bottom','.5rem').' !default;

$input-padding-y:                       '.$params->get('input-padding-y','$input-btn-padding-y').' !default;
$input-padding-x:                       '.$params->get('input-padding-x','$input-btn-padding-x').' !default;
$input-font-family:                     '.$params->get('input-font-family','$input-btn-font-family').' !default;
$input-font-size:                       '.$params->get('input-font-size','$input-btn-font-size').' !default;
$input-font-weight:                     '.$params->get('input-font-weight','$font-weight-base').' !default;
$input-line-height:                     '.$params->get('input-line-height','$input-btn-line-height').' !default;

$input-padding-y-sm:                    '.$params->get('input-padding-y-sm','$input-btn-padding-y-sm').' !default;
$input-padding-x-sm:                    '.$params->get('input-padding-x-sm','$input-btn-padding-x-sm').' !default;
$input-font-size-sm:                    '.$params->get('input-font-size-sm','$input-btn-font-size-sm').' !default;
$input-line-height-sm:                  '.$params->get('input-line-height-sm','$input-btn-line-height-sm').' !default;

$input-padding-y-lg:                    '.$params->get('input-padding-y-lg','$input-btn-padding-y-lg').' !default;
$input-padding-x-lg:                    '.$params->get('input-padding-x-lg','$input-btn-padding-x-lg').' !default;
$input-font-size-lg:                    '.$params->get('input-font-size-lg','$input-btn-font-size-lg').' !default;
$input-line-height-lg:                  '.$params->get('input-line-height-lg','$input-btn-line-height-lg').' !default;

$input-bg:                              '.$params->get('input-bg','$white').' !default;
$input-disabled-bg:                     '.$params->get('input-disabled-bg','$gray-200').' !default;

$input-color:                           '.$params->get('input-color','$gray-700').' !default;
$input-border-color:                    '.$params->get('input-border-color','$gray-400').' !default;
$input-border-width:                    '.$params->get('input-border-width','$input-btn-border-width').' !default;
$input-box-shadow:                      '.$params->get('input-box-shadow','inset 0 1px 1px rgba($black, .075)').' !default;

$input-border-radius:                   '.$params->get('input-border-radius','$border-radius').' !default;
$input-border-radius-lg:                '.$params->get('input-border-radius-lg','$border-radius-lg').' !default;
$input-border-radius-sm:                '.$params->get('input-border-radius-sm','$border-radius-sm').' !default;

$input-focus-bg:                        '.$params->get('input-focus-bg','$input-bg').' !default;
$input-focus-border-color:              '.$params->get('input-focus-border-color','lighten($component-active-bg, 25%)').' !default;
$input-focus-color:                     '.$params->get('input-focus-color','$input-color').' !default;
$input-focus-width:                     '.$params->get('input-focus-width','$input-btn-focus-width').' !default;
$input-focus-box-shadow:                '.$params->get('input-focus-box-shadow','$input-btn-focus-box-shadow').' !default;

$input-placeholder-color:               '.$params->get('input-placeholder-color','$gray-600').' !default;
$input-plaintext-color:                 '.$params->get('input-plaintext-color','$body-color').' !default;

$input-height-border:                   '.$params->get('input-height-border','$input-border-width * 2').' !default;

$input-height-inner:                    '.$params->get('input-height-inner','add($input-line-height * 1em, $input-padding-y * 2)').' !default;
$input-height-inner-half:               '.$params->get('input-height-inner-half','add($input-line-height * .5em, $input-padding-y)').' !default;
$input-height-inner-quarter:            '.$params->get('input-height-inner-half','add($input-line-height * .25em, $input-padding-y / 2)').' !default;

$input-height:                          '.$params->get('input-height','add($input-line-height * 1em, add($input-padding-y * 2, $input-height-border, false))').' !default;
$input-height-sm:                       '.$params->get('input-height-sm','add($input-line-height-sm * 1em, add($input-padding-y-sm * 2, $input-height-border, false))').' !default;
$input-height-lg:                       '.$params->get('input-height-lg','add($input-line-height-lg * 1em, add($input-padding-y-lg * 2, $input-height-border, false))').' !default;

$input-transition:                      '.$params->get('input-transition','border-color .15s ease-in-out, box-shadow .15s ease-in-out').' !default;

$form-text-margin-top:                  '.$params->get('form-text-margin-top','.25rem').' !default;

$form-check-input-gutter:               '.$params->get('form-check-input-gutter','1.25rem').' !default;
$form-check-input-margin-y:             '.$params->get('form-check-input-margin-y','.3rem').' !default;
$form-check-input-margin-x:             '.$params->get('form-check-input-margin-x','.25rem').' !default;

$form-check-inline-margin-x:            '.$params->get('form-check-inline-margin-x','.75rem').' !default;
$form-check-inline-input-margin-x:      '.$params->get('form-check-inline-input-margin-x','.3125rem').' !default;

$form-grid-gutter-width:                '.$params->get('form-grid-gutter-width','10px').' !default;
$form-group-margin-bottom:              '.$params->get('form-group-margin-bottom','1rem').' !default;

$input-group-addon-color:               '.$params->get('input-group-addon-color','$input-color').' !default;
$input-group-addon-bg:                  '.$params->get('input-group-addon-bg','$gray-200').' !default;
$input-group-addon-border-color:        '.$params->get('input-group-addon-border-color','$input-border-color').' !default;

$custom-forms-transition:               '.$params->get('custom-forms-transition','background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out').' !default;

$custom-control-gutter:                 '.$params->get('custom-control-gutter','.5rem').' !default;
$custom-control-spacer-x:               '.$params->get('custom-control-spacer-x','1rem').' !default;
$custom-control-cursor:                 '.$params->get('custom-control-cursor','null').' !default;

$custom-control-indicator-size:         '.$params->get('custom-control-indicator-size','1rem').' !default;
$custom-control-indicator-bg:           '.$params->get('custom-control-indicator-bg','$input-bg').' !default;
$custom-control-indicator-bg-size:      '.$params->get('custom-control-indicator-bg-size','50% 50%').' !default;
$custom-control-indicator-box-shadow:   '.$params->get('custom-control-indicator-box-shadow','$input-box-shadow').' !default;
$custom-control-indicator-border-color: '.$params->get('custom-control-indicator-border-color','$gray-500').' !default;
$custom-control-indicator-border-width: '.$params->get('custom-control-indicator-border-width','$input-border-width').' !default;

$custom-control-label-color:            '.$params->get('custom-control-label-color','null').' !default;

$custom-control-indicator-disabled-bg:          '.$params->get('custom-control-indicator-disabled-bg','$input-disabled-bg').' !default;
$custom-control-label-disabled-color:           '.$params->get('custom-control-label-disabled-color','$gray-600').' !default;

$custom-control-indicator-checked-color:        '.$params->get('custom-control-indicator-checked-color','$component-active-color').' !default;
$custom-control-indicator-checked-bg:           '.$params->get('custom-control-indicator-checked-bg','$component-active-bg').' !default;
$custom-control-indicator-checked-disabled-bg:  '.$params->get('custom-control-indicator-checked-disabled-bg','rgba(theme-color("primary"), .5)').' !default;
$custom-control-indicator-checked-box-shadow:   '.$params->get('custom-control-indicator-checked-box-shadow','null').' !default;
$custom-control-indicator-checked-border-color: '.$params->get('custom-control-indicator-checked-border-color','$custom-control-indicator-checked-bg').' !default;

$custom-control-indicator-focus-box-shadow:     '.$params->get('custom-control-indicator-focus-box-shadow','$input-focus-box-shadow').' !default;
$custom-control-indicator-focus-border-color:   '.$params->get('custom-control-indicator-focus-border-color','$input-focus-border-color').' !default;

$custom-control-indicator-active-color:         '.$params->get('custom-control-indicator-active-color','$component-active-color').' !default;
$custom-control-indicator-active-bg:            '.$params->get('custom-control-indicator-active-bg','lighten($component-active-bg, 35%)').' !default;
$custom-control-indicator-active-box-shadow:    '.$params->get('custom-control-indicator-active-box-shadow','null').' !default;
$custom-control-indicator-active-border-color:  '.$params->get('custom-control-indicator-active-border-color','$custom-control-indicator-active-bg').' !default;

$custom-checkbox-indicator-border-radius:       '.$params->get('custom-checkbox-indicator-border-radius','$border-radius').' !default;
$custom-checkbox-indicator-icon-checked:        '.$params->get('custom-checkbox-indicator-icon-checked',"url(\"data:image/svg+xml,<svg xmlns='http://www.w3.org/2000/svg' width='8' height='8' viewBox='0 0 8 8'><path fill='#".'{$custom-control-indicator-checked-color}'."' d='M6.564.75l-3.59 3.612-1.538-1.55L0 4.26l2.974 2.99L8 2.193z'/></svg>\")").' !default;

$custom-checkbox-indicator-indeterminate-bg:            '.$params->get('custom-checkbox-indicator-indeterminate-bg','$component-active-bg').' !default;
$custom-checkbox-indicator-indeterminate-color:         '.$params->get('custom-checkbox-indicator-indeterminate-color','$custom-control-indicator-checked-color').' !default;
$custom-checkbox-indicator-icon-indeterminate:          '.$params->get('custom-checkbox-indicator-icon-indeterminate',"url(\"data:image/svg+xml,<svg xmlns='http://www.w3.org/2000/svg' width='4' height='4' viewBox='0 0 4 4'><path stroke='".'#{$custom-checkbox-indicator-indeterminate-color}'."' d='M0 2h4'/></svg>\")").' !default;
$custom-checkbox-indicator-indeterminate-box-shadow:    '.$params->get('custom-checkbox-indicator-indeterminate-box-shadow','null').' !default;
$custom-checkbox-indicator-indeterminate-border-color:  '.$params->get('custom-checkbox-indicator-indeterminate-border-color','$custom-checkbox-indicator-indeterminate-bg').' !default;

$custom-radio-indicator-border-radius:          '.$params->get('custom-radio-indicator-border-radius','50%').' !default;
$custom-radio-indicator-icon-checked:           '.$params->get('custom-radio-indicator-icon-checked',"url(\"data:image/svg+xml,<svg xmlns='http://www.w3.org/2000/svg' width='12' height='12' viewBox='-4 -4 8 8'><circle r='3' fill='".'#{$custom-control-indicator-checked-color}'."'/></svg>\")").' !default;

$custom-switch-width:                           '.$params->get('custom-switch-width','$custom-control-indicator-size * 1.75').' !default;
$custom-switch-indicator-border-radius:         '.$params->get('custom-switch-indicator-border-radius','$custom-control-indicator-size / 2').' !default;
$custom-switch-indicator-size:                  '.$params->get('custom-switch-indicator-size','subtract($custom-control-indicator-size, $custom-control-indicator-border-width * 4)').' !default;

$custom-select-padding-y:           '.$params->get('custom-select-padding-y','$input-padding-y').' !default;
$custom-select-padding-x:           '.$params->get('custom-select-padding-x','$input-padding-x').' !default;
$custom-select-font-family:         '.$params->get('custom-select-font-family','$input-font-family').' !default;
$custom-select-font-size:           '.$params->get('custom-select-font-size','$input-font-size').' !default;
$custom-select-height:              '.$params->get('custom-select-height','$input-height').' !default;
$custom-select-indicator-padding:   '.$params->get('custom-select-indicator-padding','1rem').' !default; // Extra padding to account for the presence of the background-image based indicator
$custom-select-font-weight:         '.$params->get('custom-select-font-weight','$input-font-weight').' !default;
$custom-select-line-height:         '.$params->get('custom-select-line-height','$input-line-height').' !default;
$custom-select-color:               '.$params->get('custom-select-color','$input-color').' !default;
$custom-select-disabled-color:      '.$params->get('custom-select-disabled-color','$gray-600').' !default;
$custom-select-bg:                  '.$params->get('custom-select-bg','$input-bg').' !default;
$custom-select-disabled-bg:         '.$params->get('custom-select-disabled-bg','$gray-200').' !default;
$custom-select-bg-size:             '.$params->get('custom-select-bg-size','8px 10px').' !default; // In pixels because image dimensions
$custom-select-indicator-color:     '.$params->get('custom-select-indicator-color','$gray-800').' !default;
$custom-select-indicator:           '.$params->get('custom-select-indicator',"url(\"data:image/svg+xml,<svg xmlns='http://www.w3.org/2000/svg' width='4' height='5' viewBox='0 0 4 5'><path fill='".'#{$custom-select-indicator-color}'."' d='M2 0L0 2h4zm0 5L0 3h4z'/></svg>\")").' !default;
$custom-select-background:          '.$params->get('custom-select-background','escape-svg($custom-select-indicator) no-repeat right $custom-select-padding-x center / $custom-select-bg-size').' !default; // Used so we can have multiple background elements (e.g., arrow and feedback icon)

$custom-select-feedback-icon-padding-right: '.$params->get('custom-select-feedback-icon-padding-right','add(1em * .75, (2 * $custom-select-padding-y * .75) + $custom-select-padding-x + $custom-select-indicator-padding)').' !default;
$custom-select-feedback-icon-position:      '.$params->get('custom-select-feedback-icon-position','center right ($custom-select-padding-x + $custom-select-indicator-padding)').' !default;
$custom-select-feedback-icon-size:          '.$params->get('custom-select-feedback-icon-size','$input-height-inner-half $input-height-inner-half').' !default;

$custom-select-border-width:        '.$params->get('custom-select-border-width','$input-border-width').' !default;
$custom-select-border-color:        '.$params->get('custom-select-border-color','$input-border-color').' !default;
$custom-select-border-radius:       '.$params->get('custom-select-border-radius','$border-radius').' !default;
$custom-select-box-shadow:          '.$params->get('custom-select-box-shadow','inset 0 1px 2px rgba($black, .075)').' !default;

$custom-select-focus-border-color:  '.$params->get('custom-select-focus-border-color','$input-focus-border-color').' !default;
$custom-select-focus-width:         '.$params->get('custom-select-focus-width','$input-focus-width').' !default;
$custom-select-focus-box-shadow:    '.$params->get('custom-select-focus-box-shadow','0 0 0 $custom-select-focus-width $input-btn-focus-color').' !default;

$custom-select-padding-y-sm:        '.$params->get('custom-select-padding-y-sm','$input-padding-y-sm').' !default;
$custom-select-padding-x-sm:        '.$params->get('custom-select-padding-x-sm','$input-padding-x-sm').' !default;
$custom-select-font-size-sm:        '.$params->get('custom-select-font-size-sm','$input-font-size-sm').' !default;
$custom-select-height-sm:           '.$params->get('custom-select-height-sm','$input-height-sm').' !default;

$custom-select-padding-y-lg:        '.$params->get('custom-select-padding-y-lg','$input-padding-y-lg').' !default;
$custom-select-padding-x-lg:        '.$params->get('custom-select-padding-x-lg','$input-padding-x-lg').' !default;
$custom-select-font-size-lg:        '.$params->get('custom-select-font-size-lg','$input-font-size-lg').' !default;
$custom-select-height-lg:           '.$params->get('custom-select-height-lg','$input-height-lg').' !default;

$custom-range-track-width:          '.$params->get('custom-range-track-width','100%').' !default;
$custom-range-track-height:         '.$params->get('custom-range-track-height','.5rem').' !default;
$custom-range-track-cursor:         '.$params->get('custom-range-track-cursor','pointer').' !default;
$custom-range-track-bg:             '.$params->get('custom-range-track-bg','$gray-300').' !default;
$custom-range-track-border-radius:  '.$params->get('custom-range-track-border-radius','1rem').' !default;
$custom-range-track-box-shadow:     '.$params->get('custom-range-track-box-shadow','inset 0 .25rem .25rem rgba($black, .1)').' !default;

$custom-range-thumb-width:                   '.$params->get('custom-range-thumb-width','1rem').' !default;
$custom-range-thumb-height:                  '.$params->get('custom-range-thumb-height','$custom-range-thumb-width').' !default;
$custom-range-thumb-bg:                      '.$params->get('custom-range-thumb-bg','$component-active-bg').' !default;
$custom-range-thumb-border:                  '.$params->get('custom-range-thumb-border','0').' !default;
$custom-range-thumb-border-radius:           '.$params->get('custom-range-thumb-border-radius','1rem').' !default;
$custom-range-thumb-box-shadow:              '.$params->get('custom-range-thumb-box-shadow','0 .1rem .25rem rgba($black, .1)').' !default;
$custom-range-thumb-focus-box-shadow:        '.$params->get('custom-range-thumb-focus-box-shadow','0 0 0 1px $body-bg, $input-btn-focus-box-shadow').' !default;
$custom-range-thumb-focus-box-shadow-width:  '.$params->get('custom-range-thumb-focus-box-shadow-width','$input-focus-width').' !default; // For focus box shadow issue in IE/Edge
$custom-range-thumb-active-bg:               '.$params->get('custom-range-thumb-active-bg','lighten($component-active-bg, 35%)').' !default;
$custom-range-thumb-disabled-bg:             '.$params->get('custom-range-thumb-disabled-bg','$gray-500').' !default;

$custom-file-height:                '.$params->get('custom-file-height','$input-height').' !default;
$custom-file-height-inner:          '.$params->get('custom-file-height-inner','$input-height-inner').' !default;
$custom-file-focus-border-color:    '.$params->get('custom-file-focus-border-color','$input-focus-border-color').' !default;
$custom-file-focus-box-shadow:      '.$params->get('custom-file-focus-box-shadow','$input-focus-box-shadow').' !default;
$custom-file-disabled-bg:           '.$params->get('custom-file-disabled-bg','$input-disabled-bg').' !default;

$custom-file-padding-y:             '.$params->get('custom-file-padding-y','$input-padding-y').' !default;
$custom-file-padding-x:             '.$params->get('custom-file-padding-x','$input-padding-x').' !default;
$custom-file-line-height:           '.$params->get('custom-file-line-height','$input-line-height').' !default;
$custom-file-font-family:           '.$params->get('custom-file-font-family','$input-font-family').' !default;
$custom-file-font-weight:           '.$params->get('custom-file-font-weight','$input-font-weight').' !default;
$custom-file-color:                 '.$params->get('custom-file-color','$input-color').' !default;
$custom-file-bg:                    '.$params->get('custom-file-bg','$input-bg').' !default;
$custom-file-border-width:          '.$params->get('custom-file-border-width','$input-border-width').' !default;
$custom-file-border-color:          '.$params->get('custom-file-border-color','$input-border-color').' !default;
$custom-file-border-radius:         '.$params->get('custom-file-border-radius','$input-border-radius').' !default;
$custom-file-box-shadow:            '.$params->get('custom-file-box-shadow','$input-box-shadow').' !default;
$custom-file-button-color:          '.$params->get('custom-file-button-color','$custom-file-color').' !default;
$custom-file-button-bg:             '.$params->get('custom-file-button-bg','$input-group-addon-bg').' !default;
$custom-file-text: ('.PHP_EOL;
$lastCustomFileText = end($CustomFileText);
foreach ($CustomFileText as $value)
{
	$scssSource	.= '  '.$value['lang'].': "'.$value['text'].($lastCustomFileText !== $value ? '",' : '"').PHP_EOL;
}
$scssSource	.= ') !default;


// Form validation
$form-feedback-margin-top:          '.$params->get('form-feedback-margin-top','$form-text-margin-top').' !default;
$form-feedback-font-size:           '.$params->get('form-feedback-font-size','$small-font-size').' !default;
$form-feedback-valid-color:         '.$params->get('form-feedback-valid-color','theme-color("success")').' !default;
$form-feedback-invalid-color:       '.$params->get('form-feedback-invalid-color','theme-color("danger")').' !default;
 
$form-feedback-icon-valid-color:    '.$params->get('form-feedback-icon-valid-color','$form-feedback-valid-color').' !default;
$form-feedback-icon-valid:          '.$params->get('form-feedback-icon-valid',"url(\"data:image/svg+xml,<svg xmlns='http://www.w3.org/2000/svg' width='8' height='8' viewBox='0 0 8 8'><path fill='".'#{$form-feedback-icon-valid-color}'."' d='M2.3 6.73L.6 4.53c-.4-1.04.46-1.4 1.1-.8l1.1 1.4 3.4-3.8c.6-.63 1.6-.27 1.2.7l-4 4.6c-.43.5-.8.4-1.1.1z'/></svg>\")").' !default;
$form-feedback-icon-invalid-color:  '.$params->get('form-feedback-icon-invalid-color','$form-feedback-invalid-color').' !default;
$form-feedback-icon-invalid:        '.$params->get('form-feedback-icon-invalid',"url(\"data:image/svg+xml,<svg xmlns='http://www.w3.org/2000/svg' width='12' height='12' fill='none' stroke='".'#{$form-feedback-icon-invalid-color}'."' viewBox='0 0 12 12'><circle cx='6' cy='6' r='4.5'/><path stroke-linejoin='round' d='M5.8 3.6h.4L6 6.5z'/><circle cx='6' cy='8.2' r='.6' fill='".'#{$form-feedback-icon-invalid-color}'."' stroke='none'/></svg>\")").' !default;

$form-validation-states: () !default;
// stylelint-disable-next-line scss/dollar-variable-default
$form-validation-states: map-merge(
  (
    "valid": (
      "color": $form-feedback-valid-color,
      "icon": $form-feedback-icon-valid
    ),
    "invalid": (
      "color": $form-feedback-invalid-color,
      "icon": $form-feedback-icon-invalid
    ),
  ),
  $form-validation-states
);

// Z-index master list
//
// Warning: Avoid customizing these values. They\'re used for a bird\'s eye view
// of components dependent on the z-axis and are designed to all work together.

$zindex-dropdown:                   1000 !default;
$zindex-sticky:                     1020 !default;
$zindex-fixed:                      1030 !default;
$zindex-modal-backdrop:             1040 !default;
$zindex-modal:                      1050 !default;
$zindex-popover:                    1060 !default;
$zindex-tooltip:                    1070 !default;


// Navs

$nav-link-padding-y:                '.$params->get('nav-link-padding-y','.5rem').' !default;
$nav-link-padding-x:                '.$params->get('nav-link-padding-x','1rem').' !default;
$nav-link-disabled-color:           '.$params->get('nav-link-disabled-color','$gray-600').' !default;

$nav-tabs-border-color:             '.$params->get('nav-tabs-border-color','$gray-300').' !default;
$nav-tabs-border-width:             '.$params->get('nav-tabs-border-width','$border-width').' !default;
$nav-tabs-border-radius:            '.$params->get('nav-tabs-border-radius','$border-radius').' !default;
$nav-tabs-link-hover-border-color:  '.$params->get('nav-tabs-link-hover-border-color','$gray-200 $gray-200 $nav-tabs-border-color').' !default;
$nav-tabs-link-active-color:        '.$params->get('nav-tabs-link-active-color','$gray-700').' !default;
$nav-tabs-link-active-bg:           '.$params->get('nav-tabs-link-active-bg','$body-bg').' !default;
$nav-tabs-link-active-border-color: '.$params->get('nav-tabs-link-active-border-color','$gray-300 $gray-300 $nav-tabs-link-active-bg').' !default;

$nav-pills-border-radius:           '.$params->get('nav-pills-border-radius','$border-radius').' !default;
$nav-pills-link-active-color:       '.$params->get('nav-pills-link-active-color','$component-active-color').' !default;
$nav-pills-link-active-bg:          '.$params->get('nav-pills-link-active-bg','$component-active-bg').' !default;

$nav-divider-color:                 '.$params->get('nav-divider-color','$gray-200').' !default;
$nav-divider-margin-y:              '.$params->get('nav-divider-margin-y','$spacer / 2').' !default;

// Navbar

$navbar-padding-y:                  '.$params->get('navbar-padding-y','$spacer / 2').' !default;
$navbar-padding-x:                  '.$params->get('navbar-padding-x','$spacer').' !default;

$navbar-nav-link-padding-x:         '.$params->get('navbar-nav-link-padding-x','.5rem').' !default;

$navbar-brand-font-size:            '.$params->get('navbar-brand-font-size','$font-size-lg').' !default;
// Compute the navbar-brand padding-y so the navbar-brand will have the same height as navbar-text and nav-link
$nav-link-height:                   '.$params->get('nav-link-height','$font-size-base * $line-height-base + $nav-link-padding-y * 2').' !default;
$navbar-brand-height:               '.$params->get('navbar-brand-height','$navbar-brand-font-size * $line-height-base').' !default;
$navbar-brand-padding-y:            '.$params->get('navbar-brand-padding-y','($nav-link-height - $navbar-brand-height) / 2').' !default;

$navbar-toggler-padding-y:          '.$params->get('navbar-toggler-padding-y','.25rem').' !default;
$navbar-toggler-padding-x:          '.$params->get('navbar-toggler-padding-x','.75rem').' !default;
$navbar-toggler-font-size:          '.$params->get('navbar-toggler-font-size','$font-size-lg').' !default;
$navbar-toggler-border-radius:      '.$params->get('navbar-toggler-border-radius','$btn-border-radius').' !default;

$navbar-dark-color:                 '.$params->get('navbar-dark-color','rgba($white, .5)').' !default;
$navbar-dark-hover-color:           '.$params->get('navbar-dark-hover-color','rgba($white, .75)').' !default;
$navbar-dark-active-color:          '.$params->get('navbar-dark-active-color','$white').' !default;
$navbar-dark-disabled-color:        '.$params->get('navbar-dark-disabled-color','rgba($white, .25)').' !default;
$navbar-dark-toggler-icon-bg:       '.$params->get('navbar-dark-toggler-icon-bg',"url(\"data:image/svg+xml,<svg xmlns='http://www.w3.org/2000/svg' width='30' height='30' viewBox='0 0 30 30'><path stroke='".'#{$navbar-dark-color}'."' stroke-linecap='round' stroke-miterlimit='10' stroke-width='2' d='M4 7h22M4 15h22M4 23h22'/></svg>\")").' !default;
$navbar-dark-toggler-border-color:  '.$params->get('navbar-dark-toggler-border-color','rgba($white, .1)').' !default;

$navbar-light-color:                '.$params->get('navbar-light-color','rgba($black, .5)').' !default;
$navbar-light-hover-color:          '.$params->get('navbar-light-hover-color','rgba($black, .7)').' !default;
$navbar-light-active-color:         '.$params->get('navbar-light-active-color','rgba($black, .9)').' !default;
$navbar-light-disabled-color:       '.$params->get('navbar-light-disabled-color','rgba($black, .3)').' !default;
$navbar-light-toggler-icon-bg:      '.$params->get('navbar-light-toggler-icon-bg',"url(\"data:image/svg+xml,<svg xmlns='http://www.w3.org/2000/svg' width='30' height='30' viewBox='0 0 30 30'><path stroke='".'#{$navbar-light-color}'."' stroke-linecap='round' stroke-miterlimit='10' stroke-width='2' d='M4 7h22M4 15h22M4 23h22'/></svg>\")").' !default;
$navbar-light-toggler-border-color: '.$params->get('navbar-light-toggler-border-color','rgba($black, .1)').' !default;

$navbar-light-brand-color:                '.$params->get('navbar-light-color','$navbar-light-active-color').' !default;
$navbar-light-brand-hover-color:          '.$params->get('navbar-light-color','$navbar-light-active-color').' !default;
$navbar-dark-brand-color:                 '.$params->get('navbar-light-color','$navbar-dark-active-color').' !default;
$navbar-dark-brand-hover-color:           '.$params->get('navbar-light-color','$navbar-dark-active-color').' !default;


// Dropdowns
//
// Dropdown menu container and contents.

$dropdown-min-width:                '.$params->get('dropdown-min-width','10rem').' !default;
$dropdown-padding-y:                '.$params->get('dropdown-padding-y','.5rem').' !default;
$dropdown-spacer:                   '.$params->get('dropdown-spacer','.125rem').' !default;
$dropdown-font-size:                '.$params->get('dropdown-font-size','$font-size-base').' !default;
$dropdown-color:                    '.$params->get('dropdown-color','$body-color').' !default;
$dropdown-bg:                       '.$params->get('dropdown-bg','$white').' !default;
$dropdown-border-color:             '.$params->get('dropdown-border-color','rgba($black, .15)').' !default;
$dropdown-border-radius:            '.$params->get('dropdown-border-radius','$border-radius').' !default;
$dropdown-border-width:             '.$params->get('dropdown-border-width','$border-width').' !default;
$dropdown-inner-border-radius:      '.$params->get('dropdown-inner-border-radius','subtract($dropdown-border-radius, $dropdown-border-width)').' !default;
$dropdown-divider-bg:               '.$params->get('dropdown-divider-bg','$gray-200').' !default;
$dropdown-divider-margin-y:         '.$params->get('dropdown-divider-margin-y','$nav-divider-margin-y').' !default;
$dropdown-box-shadow:               '.$params->get('dropdown-box-shadow','0 .5rem 1rem rgba($black, .175)').' !default;

$dropdown-link-color:               '.$params->get('dropdown-link-color','$gray-900').' !default;
$dropdown-link-hover-color:         '.$params->get('dropdown-link-hover-color','darken($gray-900, 5%)').' !default;
$dropdown-link-hover-bg:            '.$params->get('dropdown-link-hover-bg','$gray-100').' !default;

$dropdown-link-active-color:        '.$params->get('dropdown-link-active-color','$component-active-color').' !default;
$dropdown-link-active-bg:           '.$params->get('dropdown-link-active-bg','$component-active-bg').' !default;

$dropdown-link-disabled-color:      '.$params->get('dropdown-link-disabled-color','$gray-600').' !default;

$dropdown-item-padding-y:           '.$params->get('dropdown-item-padding-y','.25rem').' !default;
$dropdown-item-padding-x:           '.$params->get('dropdown-item-padding-x','1.5rem').' !default;

$dropdown-header-color:             '.$params->get('dropdown-header-color','$gray-600').' !default;
$dropdown-header-padding:           '.$params->get('dropdown-header-padding','$dropdown-padding-y $dropdown-item-padding-x').' !default;


// Pagination

$pagination-padding-y:              '.$params->get('pagination-padding-y','.5rem').' !default;
$pagination-padding-x:              '.$params->get('pagination-padding-x','.75rem').' !default;
$pagination-padding-y-sm:           '.$params->get('pagination-padding-y-sm','.25rem').' !default;
$pagination-padding-x-sm:           '.$params->get('pagination-padding-x-sm','.5rem').' !default;
$pagination-padding-y-lg:           '.$params->get('pagination-padding-y-lg','.75rem').' !default;
$pagination-padding-x-lg:           '.$params->get('pagination-padding-x-lg','1.5rem').' !default;
$pagination-line-height:            '.$params->get('pagination-line-height','1.25').' !default;

$pagination-color:                  '.$params->get('pagination-color','$link-color').' !default;
$pagination-bg:                     '.$params->get('pagination-bg','$white').' !default;
$pagination-border-width:           '.$params->get('pagination-border-width','$border-width').' !default;
$pagination-border-color:           '.$params->get('pagination-border-color','$gray-300').' !default;

$pagination-focus-box-shadow:       '.$params->get('pagination-focus-box-shadow','$input-btn-focus-box-shadow').' !default;
$pagination-focus-outline:          '.$params->get('pagination-focus-outline','0').' !default;

$pagination-hover-color:            '.$params->get('pagination-hover-color','$link-hover-color').' !default;
$pagination-hover-bg:               '.$params->get('pagination-hover-bg','$gray-200').' !default;
$pagination-hover-border-color:     '.$params->get('pagination-hover-border-color','$gray-300').' !default;

$pagination-active-color:           '.$params->get('pagination-active-color','$component-active-color').' !default;
$pagination-active-bg:              '.$params->get('pagination-active-bg','$component-active-bg').' !default;
$pagination-active-border-color:    '.$params->get('pagination-active-border-color','$pagination-active-bg').' !default;

$pagination-disabled-color:         '.$params->get('pagination-disabled-color','$gray-600').' !default;
$pagination-disabled-bg:            '.$params->get('pagination-disabled-bg','$white').' !default;
$pagination-disabled-border-color:  '.$params->get('pagination-disabled-border-color','$gray-300').' !default;


// Jumbotron

$jumbotron-padding:                 '.$params->get('jumbotron-padding','2rem').' !default;
$jumbotron-color:                   '.$params->get('jumbotron-color','null').' !default;
$jumbotron-bg:                      '.$params->get('jumbotron-bg','$gray-200').' !default;


// Cards

$card-spacer-y:                     '.$params->get('card-spacer-y','.75rem').' !default;
$card-spacer-x:                     '.$params->get('card-spacer-x','1.25rem').' !default;
$card-border-width:                 '.$params->get('card-border-width','$border-width').' !default;
$card-border-radius:                '.$params->get('card-border-radius','$border-radius').' !default;
$card-border-color:                 '.$params->get('card-border-color','rgba($black, .125)').' !default;
$card-inner-border-radius:          '.$params->get('card-inner-border-radius','subtract($card-border-radius, $card-border-width)').' !default;
$card-cap-bg:                       '.$params->get('card-cap-bg','rgba($black, .03)').' !default;
$card-cap-color:                    '.$params->get('card-cap-color','null').' !default;
$card-height:                       '.$params->get('card-height','null').' !default;
$card-color:                        '.$params->get('card-color','null').' !default;
$card-bg:                           '.$params->get('card-bg','$white').' !default;

$card-img-overlay-padding:          '.$params->get('card-img-overlay-padding','1.25rem').' !default;

$card-group-margin:                 '.$params->get('card-group-margin','$grid-gutter-width / 2').' !default;
$card-deck-margin:                  '.$params->get('card-deck-margin','$card-group-margin').' !default;

$card-columns-count:                '.$params->get('card-columns-count','3').' !default;
$card-columns-gap:                  '.$params->get('card-columns-gap','1.25rem').' !default;
$card-columns-margin:               '.$params->get('card-columns-margin','$card-spacer-y').' !default;


// Tooltips

$tooltip-font-size:                 '.$params->get('tooltip-font-size','$font-size-sm').' !default;
$tooltip-max-width:                 '.$params->get('tooltip-max-width','200px').' !default;
$tooltip-color:                     '.$params->get('tooltip-color','$white').' !default;
$tooltip-bg:                        '.$params->get('tooltip-bg','$black').' !default;
$tooltip-border-radius:             '.$params->get('tooltip-border-radius','$border-radius').' !default;
$tooltip-opacity:                   '.$params->get('tooltip-opacity','.9').' !default;
$tooltip-padding-y:                 '.$params->get('tooltip-padding-y','.25rem').' !default;
$tooltip-padding-x:                 '.$params->get('tooltip-padding-x','.5rem').' !default;
$tooltip-margin:                    '.$params->get('tooltip-margin','0').' !default;

$tooltip-arrow-width:               '.$params->get('tooltip-arrow-width','.8rem').' !default;
$tooltip-arrow-height:              '.$params->get('tooltip-arrow-height','.4rem').' !default;
$tooltip-arrow-color:               '.$params->get('tooltip-arrow-color','$tooltip-bg').' !default;

// Form tooltips must come after regular tooltips
$form-feedback-tooltip-padding-y:     '.$params->get('form-feedback-tooltip-padding-y','$tooltip-padding-y').' !default;
$form-feedback-tooltip-padding-x:     '.$params->get('form-feedback-tooltip-padding-x','$tooltip-padding-x').' !default;
$form-feedback-tooltip-font-size:     '.$params->get('form-feedback-tooltip-font-size','$tooltip-font-size').' !default;
$form-feedback-tooltip-line-height:   '.$params->get('form-feedback-tooltip-line-height','$line-height-base').' !default;
$form-feedback-tooltip-opacity:       '.$params->get('form-feedback-tooltip-opacity','$tooltip-opacity').' !default;
$form-feedback-tooltip-border-radius: '.$params->get('form-feedback-tooltip-border-radius','$tooltip-border-radius').' !default;


// Popovers

$popover-font-size:                 '.$params->get('popover-font-size','$font-size-sm').' !default;
$popover-bg:                        '.$params->get('popover-bg','$white').' !default;
$popover-max-width:                 '.$params->get('popover-max-width','276px').' !default;
$popover-border-width:              '.$params->get('popover-border-width','$border-width').' !default;
$popover-border-color:              '.$params->get('popover-border-color','rgba($black, .2)').' !default;
$popover-border-radius:             '.$params->get('popover-border-radius','$border-radius-lg').' !default;
$popover-inner-border-radius:       '.$params->get('popover-inner-border-radius','subtract($popover-border-radius, $popover-border-width)').' !default;
$popover-box-shadow:                '.$params->get('popover-box-shadow','0 .25rem .5rem rgba($black, .2)').' !default;

$popover-header-bg:                 '.$params->get('popover-header-bg','darken($popover-bg, 3%)').' !default;
$popover-header-color:              '.$params->get('popover-header-color','$headings-color').' !default;
$popover-header-padding-y:          '.$params->get('popover-header-padding-y','.5rem').' !default;
$popover-header-padding-x:          '.$params->get('popover-header-padding-x','.75rem').' !default;

$popover-body-color:                '.$params->get('popover-body-color','$body-color').' !default;
$popover-body-padding-y:            '.$params->get('popover-body-padding-y','$popover-header-padding-y').' !default;
$popover-body-padding-x:            '.$params->get('popover-body-padding-x','$popover-header-padding-x').' !default;

$popover-arrow-width:               '.$params->get('popover-arrow-width','1rem').' !default;
$popover-arrow-height:              '.$params->get('popover-arrow-height','.5rem').' !default;
$popover-arrow-color:               '.$params->get('popover-arrow-color','$popover-bg').' !default;

$popover-arrow-outer-color:         '.$params->get('popover-arrow-outer-color','fade-in($popover-border-color, .05)').' !default;


// Toasts
$toast-max-width:                   '.$params->get('toast-max-width','350px').' !default;
$toast-padding-x:                   '.$params->get('toast-padding-x','.75rem').' !default;
$toast-padding-y:                   '.$params->get('toast-padding-y','.25rem').' !default;
$toast-font-size:                   '.$params->get('toast-font-size','.875rem').' !default;
$toast-color:                       '.$params->get('toast-color','null').' !default;
$toast-background-color:            '.$params->get('toast-background-color','rgba($white, .85)').' !default;
$toast-border-width:                '.$params->get('toast-border-width','1px').' !default;
$toast-border-color:                '.$params->get('toast-border-color','rgba(0, 0, 0, .1)').' !default;
$toast-border-radius:               '.$params->get('toast-border-radius','.25rem').' !default;
$toast-box-shadow:                  '.$params->get('toast-box-shadow','0 .25rem .75rem rgba($black, .1)').' !default;

$toast-header-color:                '.$params->get('toast-header-color','$gray-600').' !default;
$toast-header-background-color:     '.$params->get('toast-header-background-color','rgba($white, .85)').' !default;
$toast-header-border-color:         '.$params->get('toast-header-border-color','rgba(0, 0, 0, .05)').' !default;


// Badges

$badge-font-size:                   '.$params->get('badge-font-size','75%').' !default;
$badge-font-weight:                 '.$params->get('badge-font-weight','$font-weight-bold').' !default;
$badge-padding-y:                   '.$params->get('badge-padding-y','.25em').' !default;
$badge-padding-x:                   '.$params->get('badge-padding-x','.4em').' !default;
$badge-border-radius:               '.$params->get('badge-border-radius','$border-radius').' !default;

$badge-transition:                  '.$params->get('badge-transition','$btn-transition').' !default;
$badge-focus-width:                 '.$params->get('badge-focus-width','$input-btn-focus-width').' !default;

$badge-pill-padding-x:              '.$params->get('badge-pill-padding-x','.6em').' !default;
// Use a higher than normal value to ensure completely rounded edges when
// customizing padding or font-size on labels.
$badge-pill-border-radius:          '.$params->get('badge-pill-border-radius','10rem').' !default;


// Modals

// Padding applied to the modal body
$modal-inner-padding:               '.$params->get('modal-inner-padding','1rem').' !default;

// Margin between elements in footer, must be lower than or equal to 2 * $modal-inner-padding
$modal-footer-margin-between:       '.$params->get('modal-footer-margin-between','.5rem').' !default;

$modal-dialog-margin:               '.$params->get('modal-dialog-margin','.5rem').' !default;
$modal-dialog-margin-y-sm-up:       '.$params->get('modal-dialog-margin-y-sm-up','1.75rem').' !default;

$modal-title-line-height:           '.$params->get('modal-title-line-height','$line-height-base').' !default;

$modal-content-color:               '.$params->get('modal-content-color','null').' !default;
$modal-content-bg:                  '.$params->get('modal-content-bg','$white').' !default;
$modal-content-border-color:        '.$params->get('modal-content-border-color','rgba($black, .2)').' !default;
$modal-content-border-width:        '.$params->get('modal-content-border-width','$border-width').' !default;
$modal-content-border-radius:       '.$params->get('modal-content-border-radius','$border-radius-lg').' !default;
$modal-content-inner-border-radius: '.$params->get('modal-content-inner-border-radius','subtract($modal-content-border-radius, $modal-content-border-width)').' !default;
$modal-content-box-shadow-xs:       '.$params->get('modal-content-box-shadow-xs','0 .25rem .5rem rgba($black, .5)').' !default;
$modal-content-box-shadow-sm-up:    '.$params->get('modal-content-box-shadow-sm-up','0 .5rem 1rem rgba($black, .5)').' !default;

$modal-backdrop-bg:                 '.$params->get('modal-backdrop-bg','$black').' !default;
$modal-backdrop-opacity:            '.$params->get('modal-backdrop-opacity','.5').' !default;
$modal-header-border-color:         '.$params->get('modal-header-border-color','$border-color').' !default;
$modal-footer-border-color:         '.$params->get('modal-footer-border-color','$modal-header-border-color').' !default;
$modal-header-border-width:         '.$params->get('modal-header-border-width','$modal-content-border-width').' !default;
$modal-footer-border-width:         '.$params->get('modal-footer-border-width','$modal-header-border-width').' !default;
$modal-header-padding-y:            '.$params->get('modal-header-padding-y','1rem').' !default;
$modal-header-padding-x:            '.$params->get('modal-header-padding-x','1rem').' !default;
$modal-header-padding:              '.$params->get('modal-header-padding','$modal-header-padding-y $modal-header-padding-x').' !default; // Keep this for backwards compatibility

$modal-xl:                          '.$params->get('modal-xl','1140px').' !default;
$modal-lg:                          '.$params->get('modal-lg','800px').' !default;
$modal-md:                          '.$params->get('modal-md','500px').' !default;
$modal-sm:                          '.$params->get('modal-sm','300px').' !default;

$modal-fade-transform:              '.$params->get('modal-fade-transform','translate(0, -50px)').' !default;
$modal-show-transform:              '.$params->get('modal-show-transform','none').' !default;
$modal-transition:                  '.$params->get('modal-transition','transform .3s ease-out').' !default;
$modal-scale-transform:             '.$params->get('modal-scale-transform','scale(1.02)').' !default;


// Alerts
//
// Define alert colors, border radius, and padding.

$alert-padding-y:                   '.$params->get('alert-padding-y','.75rem').' !default;
$alert-padding-x:                   '.$params->get('alert-padding-x','1.25rem').' !default;
$alert-margin-bottom:               '.$params->get('alert-margin-bottom','1rem').' !default;
$alert-border-radius:               '.$params->get('alert-border-radius','$border-radius').' !default;
$alert-link-font-weight:            '.$params->get('alert-link-font-weight','$font-weight-bold').' !default;
$alert-border-width:                '.$params->get('alert-border-width','$border-width').' !default;

$alert-bg-level:                    '.$params->get('alert-bg-level','-10').' !default;
$alert-border-level:                '.$params->get('alert-border-level','-9').' !default;
$alert-color-level:                 '.$params->get('alert-color-level','6').' !default;


// Progress bars

$progress-height:                   '.$params->get('progress-height','1rem').' !default;
$progress-font-size:                '.$params->get('progress-font-size','$font-size-base * .75').' !default;
$progress-bg:                       '.$params->get('progress-bg','$gray-200').' !default;
$progress-border-radius:            '.$params->get('progress-border-radius','$border-radius').' !default;
$progress-box-shadow:               '.$params->get('progress-box-shadow','inset 0 .1rem .1rem rgba($black, .1)').' !default;
$progress-bar-color:                '.$params->get('progress-bar-color','$white').' !default;
$progress-bar-bg:                   '.$params->get('progress-bar-bg','theme-color("primary")').' !default;
$progress-bar-animation-timing:     '.$params->get('progress-bar-animation-timing','1s linear infinite').' !default;
$progress-bar-transition:           '.$params->get('progress-bar-transition','width .6s ease').' !default;


// List group

$list-group-color:                  '.$params->get('list-group-color','null').' !default;
$list-group-bg:                     '.$params->get('list-group-bg','$white').' !default;
$list-group-border-color:           '.$params->get('list-group-border-color','rgba($black, .125)').' !default;
$list-group-border-width:           '.$params->get('list-group-border-width','$border-width').' !default;
$list-group-border-radius:          '.$params->get('list-group-border-radius','$border-radius').' !default;

$list-group-item-padding-y:         '.$params->get('list-group-item-padding-y','.75rem').' !default;
$list-group-item-padding-x:         '.$params->get('list-group-item-padding-x','1.25rem').' !default;

$list-group-hover-bg:               '.$params->get('list-group-hover-bg','$gray-100').' !default;
$list-group-active-color:           '.$params->get('list-group-active-color','$component-active-color').' !default;
$list-group-active-bg:              '.$params->get('list-group-active-bg','$component-active-bg').' !default;
$list-group-active-border-color:    '.$params->get('list-group-active-border-color','$list-group-active-bg').' !default;

$list-group-disabled-color:         '.$params->get('list-group-disabled-color','$gray-600').' !default;
$list-group-disabled-bg:            '.$params->get('list-group-disabled-bg','$list-group-bg').' !default;

$list-group-action-color:           '.$params->get('list-group-action-color','$gray-700').' !default;
$list-group-action-hover-color:     '.$params->get('list-group-action-hover-color','$list-group-action-color').' !default;

$list-group-action-active-color:    '.$params->get('list-group-action-active-color','$body-color').' !default;
$list-group-action-active-bg:       '.$params->get('list-group-action-active-bg','$gray-200').' !default;


// Image thumbnails

$thumbnail-padding:                 '.$params->get('thumbnail-padding','.25rem').' !default;
$thumbnail-bg:                      '.$params->get('thumbnail-bg','$body-bg').' !default;
$thumbnail-border-width:            '.$params->get('thumbnail-border-width','$border-width').' !default;
$thumbnail-border-color:            '.$params->get('thumbnail-border-color','$gray-300').' !default;
$thumbnail-border-radius:           '.$params->get('thumbnail-border-radius','$border-radius').' !default;
$thumbnail-box-shadow:              '.$params->get('thumbnail-box-shadow','0 1px 2px rgba($black, .075)').' !default;


// Figures

$figure-caption-font-size:          '.$params->get('figure-caption-font-size','90%').' !default;
$figure-caption-color:              '.$params->get('figure-caption-color','$gray-600').' !default;


// Breadcrumbs

$breadcrumb-font-size:              '.$params->get('breadcrumb-font-size','null').' !default;

$breadcrumb-padding-y:              '.$params->get('breadcrumb-padding-y','.75rem').' !default;
$breadcrumb-padding-x:              '.$params->get('breadcrumb-padding-x','1rem').' !default;
$breadcrumb-item-padding:           '.$params->get('breadcrumb-item-padding','.5rem').' !default;

$breadcrumb-margin-bottom:          '.$params->get('breadcrumb-margin-bottom','1rem').' !default;

$breadcrumb-bg:                     '.$params->get('breadcrumb-bg','$gray-200').' !default;
$breadcrumb-divider-color:          '.$params->get('breadcrumb-divider-color','$gray-600').' !default;
$breadcrumb-active-color:           '.$params->get('breadcrumb-active-color','$gray-600').' !default;
$breadcrumb-divider:                '.$params->get('breadcrumb-divider','quote("/")').' !default;

$breadcrumb-border-radius:          '.$params->get('breadcrumb-border-radius','$border-radius').' !default;


// Carousel

$carousel-control-color:             '.$params->get('carousel-control-color','$white').' !default;
$carousel-control-width:             '.$params->get('carousel-control-width','15%').' !default;
$carousel-control-opacity:           '.$params->get('carousel-control-opacity','.5').' !default;
$carousel-control-hover-opacity:     '.$params->get('carousel-control-hover-opacity','.9').' !default;
$carousel-control-transition:        '.$params->get('carousel-control-transition','opacity .15s ease').' !default;

$carousel-indicator-width:           '.$params->get('carousel-indicator-width','30px').' !default;
$carousel-indicator-height:          '.$params->get('carousel-indicator-height','3px').' !default;
$carousel-indicator-hit-area-height: '.$params->get('carousel-indicator-hit-area-height','10px').' !default;
$carousel-indicator-spacer:          '.$params->get('carousel-indicator-spacer','3px').' !default;
$carousel-indicator-active-bg:       '.$params->get('carousel-indicator-active-bg','$white').' !default;
$carousel-indicator-transition:      '.$params->get('carousel-indicator-transition','opacity .6s ease').' !default;

$carousel-caption-width:             '.$params->get('carousel-caption-width','70%').' !default;
$carousel-caption-color:             '.$params->get('carousel-caption-color','$white').' !default;

$carousel-control-icon-width:        '.$params->get('carousel-control-icon-width','20px').' !default;

$carousel-control-prev-icon-bg:      '.$params->get('carousel-control-prev-icon-bg',"url(\"data:image/svg+xml,<svg xmlns='http://www.w3.org/2000/svg' fill='".'#{$carousel-control-color}'."' width='8' height='8' viewBox='0 0 8 8'><path d='M5.25 0l-4 4 4 4 1.5-1.5L4.25 4l2.5-2.5L5.25 0z'/></svg>\")").' !default;
$carousel-control-next-icon-bg:      '.$params->get('carousel-control-next-icon-bg',"url(\"data:image/svg+xml,<svg xmlns='http://www.w3.org/2000/svg' fill='".'#{$carousel-control-color}'."' width='8' height='8' viewBox='0 0 8 8'><path d='M2.75 0l-1.5 1.5L3.75 4l-2.5 2.5L2.75 8l4-4-4-4z'/></svg>\")").' !default;

$carousel-transition-duration:       '.$params->get('carousel-transition-duration','.6s').'  !default;
$carousel-transition:                '.$params->get('carousel-transition','transform $carousel-transition-duration ease-in-out').' !default; // Define transform transition first if using multiple transitions (e.g., `transform 2s ease, opacity .5s ease-out`)


// Spinners

$spinner-width:         '.$params->get('spinner-width','2rem').' !default;
$spinner-height:        '.$params->get('spinner-height','$spinner-width').' !default;
$spinner-border-width:  '.$params->get('spinner-border-width','.25em').' !default;

$spinner-width-sm:        '.$params->get('spinner-width-sm','1rem').' !default;
$spinner-height-sm:       '.$params->get('spinner-height-sm','$spinner-width-sm').' !default;
$spinner-border-width-sm: '.$params->get('spinner-border-width-sm','.2em').' !default;


// Close

$close-font-size:                   '.$params->get('close-font-size','$font-size-base * 1.5').' !default;
$close-font-weight:                 '.$params->get('close-font-weight','$font-weight-bold').' !default;
$close-color:                       '.$params->get('close-color','$black').' !default;
$close-text-shadow:                 '.$params->get('close-text-shadow','0 1px 0 $white').' !default;


// Code

$code-font-size:                    '.$params->get('code-font-size','87.5%').' !default;
$code-color:                        '.$params->get('code-color','$pink').' !default;

$kbd-padding-y:                     '.$params->get('kbd-padding-y','.2rem').' !default;
$kbd-padding-x:                     '.$params->get('kbd-padding-x','.4rem').' !default;
$kbd-font-size:                     '.$params->get('kbd-font-size','$code-font-size').' !default;
$kbd-color:                         '.$params->get('kbd-color','$white').' !default;
$kbd-bg:                            '.$params->get('kbd-bg','$gray-900').' !default;

$pre-color:                         '.$params->get('pre-color','$gray-900').' !default;
$pre-scrollable-max-height:         '.$params->get('pre-scrollable-max-height','340px').' !default;


// Utilities

$displays:  '.$params->get('displays','none, inline, inline-block, block, table, table-row, table-cell, flex, inline-flex').' !default;
$overflows: '.$params->get('overflows','auto, hidden').' !default;
$positions: '.$params->get('positions','static, relative, absolute, fixed, sticky').' !default;
$user-selects: '.$params->get('user-selects','all, auto, none').' !default;


// Printing
$print-page-size:                   '.$params->get('print-page-size','a3').' !default;
$print-body-min-width:              '.$params->get('print-body-min-width','map-get($grid-breakpoints, "lg")').' !default;
';