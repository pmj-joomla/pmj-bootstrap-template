# PMJ Bootstrap Template

The PMJ Bootstrap Template is a customizable template for the Joomla! CMS

I created it for personal use but thought I could share it with the community.  
There's nothing too fancy going on in the admin area.  
I may add or remove features as I need or don't need them for my projects and workflow.  
The template will remain as is and there's is no warranty nor guaranty!  
I don't hold any liability for broken websites due to the current state of or future changes to the template!

**Note**: By default, the primary color is set to orange and the font is set to Zilla Slab to bring back some of that oldschool Rhuk Solar Flare II feeling :)

**IMPORTANT!**  
Frontend editing does not work, is not a top priority and is disabled by default!

## Table of contents

* [Download](#download)
  * [Installation](#installation)
  * [Update](#update)
* [Features](#features)
* [Is this a blank template?](#is-this-a-blank-template)
* [Third party](#third-party)
* [Notes on overrides](#notes-on-overrides)
  * [mod_menu](#mod_menu)
  * [mod_login](#mod_login)

## Download

[Current Stable Version (2.1.5](https://gitlab.com/pmj-joomla/pmj-bootstrap-template/-/archive/2.1.5/pmj-bootstrap-template-2.1.5.zip)  
[Release Notes](https://gitlab.com/pmj-joomla/pmj-bootstrap-template/-/releases/2.1.5)

[Developement Version](https://gitlab.com/pmj-joomla/pmj-bootstrap-template/-/archive/dev/pmj-bootstrap-template-dev.zip)

[All releases](https://gitlab.com/pmj-joomla/pmj-bootstrap-template/-/releases)

### Installation

To install the PMJ Bootstrap Template you can either
* download the zip file and upload it through the extension installer on your site
* copy the download link and use the "install from url" in the extension installer on your site

### Update

To update the PMJ Bootstrap Template you can either
* use the Joomla extension updater
* download the zip file and upload it through the extension installer on your site

**IMPORTANT!**  
It might be that Bootstrap changes, removes or adds new scss variables! This will lead to compilation errors when you first reload the page!
You can always check the history of the [scss/bootstrap/_variables.scss](https://gitlab.com/pmj-joomla/pmj-bootstrap-template/blob/master/scss/bootstrap/_variables.scss) file to check which variables might cause a problem!

## Features

* Customizable Grid System  
  Set rows, columns and positions how and where you like it
* Custom Fonts  
  Upload custom fonts to ommit external calls
* Custom Error Page  
  You can create your own custom 404 error page
* Bootstrap 4 Compiler  
  Customize and recompile Bootstrap on the fly.  
  **Note**: The Bootstrap SCSS is only compiled when you call a page where the template is assigned to.
* Add custom language files
* pmj-linear-gradient mixin  
  I thought the Bootstrap gradients were a bit too strictly designed so I included a more flexible gradient from [sitepoint](https://www.sitepoint.com/building-linear-gradient-mixin-sass/)
* A few overrides for Bootstrap 4 compatibility
* It has a pmjbody-$ItemID class on the body for individual page styling  
  The $ItemID is the corresponding Menu-ID for a page.  
  e.g. if your page has the menu ID 207 the css body class for this entry/page is `.pmjbody-207`

## Is this a blank template?

It can be, if you really really believe in it :)  
But seriously, you can use it as a blank template!  
There's an option in the backend to disable the PMJ render mechanism, so you can basically use it as a Bootstrap Compiler and then create your own layout in the custom_index.php.

## Third party

This template includes the following third party software

* [Bootstrap 4.5.2](https://getbootstrap.com) - for variable changes check the [CHANGELOG.md](https://gitlab.com/pmj-joomla/pmj-bootstrap-template/-/blob/master/CHANGELOG.md)
* [scssphp 1.1.1](https://github.com/scssphp/scssphp)
* [TTF Info 1.1](https://www.phpclasses.org/package/2144-PHP-Retrieve-details-from-TrueType-font-files.html)
* [-prefix-free](http://leaverou.github.io/prefixfree/)
* The following 35 Google Fonts:
  * Aclonica
  * Adamina
  * Alike Angular
  * Amethysta
  * Anton
  * Architects Daughter
  * Audiowide
  * Barrio
  * Bentham
  * Bitter
  * Butcherman
  * Caveat
  * Caveat Brush
  * Cinzel
  * Cinzel Decorative
  * Cormorant
  * Cormorant Garamond
  * Cormorant Infant
  * Cormorant SC
  * Cormorant Unicase
  * Cormorant Upright
  * Cutive Mono
  * Dosis
  * IBM Plex Mono
  * IBM Plex Serif
  * Indie Flower
  * Lekton
  * Muli
  * Neucha
  * Old Standard TT
  * Open Sans Condensed Light
  * Oswald
  * Play
  * Playfair Display
  * Playfair Display SC
  * Poiret One
  * Press Start 2P
  * PT Mono
  * Raleway
  * Roboto Slab
  * Schoolbell
  * Shadows Into Light Two
  * Share Tech Mono
  * Source Code Pro
  * Special Elite
  * The Girl Next Door
  * Uncial Antiqua
  * UnifrakturCook
  * UnifrakturMaguntia
  * VT323
  * ZCOOL Xiaowei
  * Zilla Slab
  * Zilla Slab Highlight

## Notes on overrides

I needed to create a few overrides for Bootstrap 4 compatibility and a few need special treatment now.

### mod_menu

There are several mod_menu overrides:
* pmj-navbar  
  Should be used for the navbar.
  I created the mod_menu navbar override to be compatible with normal navigation and navbar navigation.  
  Since the default class is "nav", I changed the order of the menu class sufix to the beginning of the class attribute.  
  So it's now an appendix and you can use it for a navbar by adding "navbar-" to the menu class sufix.  
  Keep in mind that any other additional classes need to go in front of it.  
  Furthermore, the mod_menu override is limited to one sub-level since Bootstrap is not designed for further nested navigations.
* pmj-navbar-usermenu
  Can and should only be used for the login dropdown menu.  
  It is one level only.
* pmj-bootstrap  
  Has just the bootstrap markup and should only be used for any single level of a menu structure.  
  It only displays component and url links.  
  `.nav-pills` can be aplied in the module settings.

**Note**: Dropdowns only work with separator entries!  
If your dropdowns don't work, check if you have a component entrie with subentries and change it!  
The console can give you a hint which entry is wrong.

### mod_login

The mod_login contains the positions "usermenu-module" and "usermenu-navbar". You can use it to add a custom usermenu when a user is logged in.  
There is a dropdown-nav layout which can be used in a navbar.  
**Note**: you can't use dropdown menus for the "usermenu-navbar" position when using the dropdown-nav layout and you need to set the menu layout to "usermenu-navbar" for a correct rendering!
