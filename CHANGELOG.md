## Changelog

### 3.0

#### 3.0.0 [2023-06-27]

* Version 3.0.0
* Changed repeatable fields to subform
* Fixed deprecated JRequest
* Fixed Call to undefined method Joomla\CMS\Installer\Adapter\TemplateAdapter::get() in installer

---
### 2.1

#### 2.1.5 [2021-06-05]

* Version 2.1.5
* Update gitignore
* Change formatting
* Fix: multi-style support with separate css compilation

#### 2.1.4 [2020-09-28]

* Version 2.1.4
* Fixed password placeholder in login module override

#### 2.1.3 [2020-09-27]

* Version 2.1.3
* Updated Readme
* Added pmjbody- class zu body

#### 2.1.2 [2020-08-16]

* Version 2.1.2
* **[BUG]** Fixed a small bug in error.php

#### 2.1.1 [2020-08-16]

* Version 2.1.1
* Updated Navbar

#### 2.1.0 [2020-08-16]

* Version 2.1.0
* Added 18 more Google Fonts
* Updated some stuff in README.md
* **[BUG]** Forgot to add the error.php in the template manifest

---
### 2.0

#### 2.0.0 [2020-08-16]

* Version 2.0.0
* **[BUG]** Syntax error
* Display some information upon installation or update
* Updated scssphp to 1.1.1
* Updated Bootstrap to 4.5.2
* **[BUG]** Forgot to add getParam() function
* Wrong version variable :(
* Custom Index was added in version 1.4.0
* **[BUG]** Accidentally deleted variable declaration for $templatePath in installer script
* Updated install script to do operations depending on versions
* Added custom error page
* Remove old mod_menu overrides
* Updated README
* Added pmj- to menu overrides
* Added single level bootstrap menu override
* Renamed navbar menu override and fixed a small bug
* Typo
* Always 'No' first
* Added language string for close

Due to the Bootstrap Update you should check the following variables and revert them to default (especially the svg reference which are marked in bold):  
* Navbar / Navs
  * **$navbar-dark-toggler-icon-bg**
  * **$navbar-light-toggler-icon-bg**
* Dropdowns / Buttons
  * $dropdown-inner-border-radius
* Forms
  * $input-height-inner
  * $input-height-inner-half
  * $input-height-inner-quarter
  * $input-height
  * $input-height-sm
  * $input-height-lg
  * $custom-control-indicator-checked-box-shadow
  * $custom-control-indicator-active-box-shadow
  * **$custom-checkbox-indicator-icon-checked**
  * **$custom-checkbox-indicator-icon-indeterminate**
  * $custom-checkbox-indicator-indeterminate-box-shadow
  * **$custom-radio-indicator-icon-checked**
  * $custom-switch-indicator-size
  * **$custom-select-indicator**
  * **$custom-select-background**
  * $custom-select-feedback-icon-padding-right
  * **$form-feedback-icon-valid**
  * **$form-feedback-icon-invalid**
* Cards / Tooltips / Popover / Toasts
  * $card-inner-border-radius
* Modals / List Group / Carousel
  * **$carousel-control-prev-icon-bg**
  * **$carousel-control-next-icon-bg**

---
### 1.9

#### 1.9.3 [2019-10-17]

* Version 1.9.3
* Added language string for nav toggler label

#### 1.9.2 [2019-10-16]

* Version 1.9.2
* Updated Matomo Code

#### 1.9.1 [2019-10-14]

* Version 1.9.1
* Typo in README
* Active state for dropdown menus (separator item)

#### 1.9.0 [2019-07-09]

* Version 1.9.0
* Update Bootstrap from 4.2.1 to 4.3.1
* Updated scssphp from 0.7.7 to 1.0.2

### 1.8

#### 1.8.3 [2019-02-22]

* Version 1.8.3
* Add PrefixFree to autoprefix css

#### 1.8.2 [2019-02-01]

* Version 1.8.2
* Eff them effing scrollbars

#### 1.8.1 [2019-01-25]

* Version 1.8.1
* Forgot to add path to css file, d'err

#### 1.8.0 [2019-01-25]

* Version 1.8.0
* Why use the easy way when there's a complicated one?
* Add the possibility to add customScript to head (default) or end with tag {end}
* Small code improvement
* Add the possibility to add custom (s)css files

---
### 1.7

#### 1.7.4 [2019-01-23]

* Version 1.7.4
* Remove hardcoded mb-1 in navbar
* Fix for Navbar due to scrollbar fix e6580e77

#### 1.7.3 [2019-02-22]

* Version 1.7.3
* Needed a different concept for custom language

#### 1.7.2 [2019-01-20]

* Version 1.7.2
* Fix comments in language files

#### 1.7.1 [2019-01-19]

* Version 1.7.1
* Add scrollbar fix
* Ok, it was a really bad concept

#### 1.7.0 [2019-01-18]

* Version 1.7.0
* Some fixes if PMJRender is disabled
* Footer flexible grid
* Header flexible grid
* Forgot to add defaults
* Make Grid more flexible (or complicated?)

---
### 1.6

#### 1.6.0 [2019-01-16]

* Version 1.6.0
* Add custom .content
* Add the possibility to add custom script files
* Removed navbar-right, navbar-collapse and usermenu positions and add usermenu-module and usermenu-navbar position
* Fix mod_login navbar layout
* Fix Navbar
* Fix Bootstrap embed aspect ratio (3by4 instead of 4by3)
* Some adjustments for mod_menu default layout
* Add mod_menu override for navbar mod_login usermenu

---
### 1.5

#### 1.5.1 [2019-01-14]

* Version 1.5.1
* Navbar 'fullcontainer' typo

#### 1.5.0 [2019-01-14]

* Version 1.5.0
* Small Fix
* Improved Navbar Customisation

---
### 1.4

#### 1.4.0 [2019-01-12]

* Version 1.4.0
* Forgot to escape string delimiter
* Remove custom_index.php to avoid overwrite on update
* Add the possibility to add custom language files
* Just casting didn't work, Joomla always returns a string for default params
* Cast default grid arrays as objects
* Update to Bootstrap 4.2.1
* Fix php warning

---
### 1.3

#### 1.3.1 [2019-01-07]

* Version 1.3.1
* Forgot to add noscript tag

#### 1.3.0 [2019-01-07]

* Version 1.3.0
* Add custom scripts
* Add Matomo
* Fixed a bug in mod_menu override
* Add viewport meta to head
* Remove favicon.ico to avoid overwriting on update
* Fix foreach warning in customcolorlist

---
### 1.2

#### 1.2.0 [2019-01-06]

* Version 1.2.0
* Add text selection color and add custom color list field
* Make custom colors available in color dropdowns

---
### 1.1

#### 1.1.1 [2019-01-06]

* Version 1.1.1
* Remove hardcoded padding from header
* Fix a small design bug in footer

#### 1.1.0 [2019-01-05]

* Version 1.1.0
* Load jquery (d'err)
* Fixed some of the footer stuff
* Add some footer parameters
* Header link sitename
* Container flex
* Fixed json_decode on wrong position
* Redesign Grid logic
* Change custom colors field type to subform

---
### 1.0

#### 1.0.1 [2019-01-02]

* Version 1.0.1
* Fixed Bootstrap font compiling
* Wasn't fixed
* Fix FontAwesome menu anchor class

#### 1.0.0 [2019-01-01]

* Version 1.0.0 Production Stable
* Fixed module render
* Some tweaks (header positions, custom navbar)

---
0.7

#### 0.7.3 [2018-12-29]

* Version 0.7.3
* Fix for Navbar width
* Small fix in template options

#### 0.7.2 [2018-12-27]

* Version 0.7.2
* Updater doesn't delete files and folders

#### 0.7.1 [2018-12-27]

* Version 0.7.1
* Bug, bug, buggy

#### 0.7.0 [2018-12-27]

* Version 0.7.0
* Comment the execution time
* Stylesheet doesn't load
* Changed a little bit of the concept
* Add install/update script
* Add pmj-linear-gradient mixin
* Add custom

---
### 0.6

#### 0.6.0 [2018-12-20]

* Version 0.6.0
* Close / Code / Printing
* Breadcrumbs
* Figures
* Image thumbnails
* Progress bars
* Alerts
* Badges
* Jumbotron
* Carousel
* List Group
* Modals
* Popovers
* Font Resizer improvement
* Tooltips
* Add Font Resizer
* Cards
* Create the last batch of variables
* Pagination
* Tables
* YIQ

---
### 0.5

#### 0.5.1 [2018-12-18]

* Version 0.5.1
* Redesign custom layout/blank template
* Add infinite custom colors

#### 0.5.0 [2018-12-17]

* Version 0.5.0
* Fixed some problems with custom font loading/display
* Remove old pmj.php
* Wrong Markup
* Blank Template
* Small Bug in Bootstrap Compiler
* Dropdowns
* Custom Form (da hell!!)
* Forms
* Buttons
* Clean up language
* Clean up templateDetails.xml

---
### 0.4

#### 0.4.1 [2018-12-14]

* Version 0.4.1
* Load stylesheet only after bootstrap has been compiled (i have a feeling that this still isn't quite right)
* Bootstrap is not compiled upon install
* Grid wasn't rendered upon install
* Fix #2

#### 0.4.0 [2018-12-14]

* Version 0.4.0
* Added a few more fonts
* Move fontlist form field
* Add Login navbar dropdown layout
* Forgot to add defaults to variables
* Add Bootstrap Grid & Components
* Fonts
* General colors
* Add Navbar
* Small fix for compiling bootstrap
* Decided to add all bootstrap variables to one file and add joomla parameters preventively
* Remove some whitespace
* README.md
* Restructure Bootstrap Compiler
* Restructure template code

---
### 0.3

#### 0.3.1 [2018-12-05]

* Version 0.3.1
* Remove .fluid from .row

#### 0.3.0 [2018-12-05]

* Some language tweaks
* Add mod_menu language
* Add mod_menu override
* Change targetplatform in update.xml
* Add LICENSE
* Update README.md
* Use original ttf class to avoid license conflict
* Forgot to add scssphp license
* Forgot to set primary color in bootstrap compiler
* Forgot to add default font to boostrap compiler
* Add link to index.php
* Slight improvement in mod_login override
* Forgot to add form folder
* Correct padding in header tag
* Add some of my favourite fonts :)
* Add the possibility to add custom fonts
* Move libraries to libs folder
* Add info_block override
* Add icons override
* Add mod_articles_popular override
* Add mod_articles_category override
* index.html for mod_login override
* Add mod_tags_popular override
* Set primary color to $orange :)
* Add mod_login override
* Disable Frontend Editing
* Enable Bootstrap Tooltips

---
### 0.2

#### 0.2.1 [2018-12-01]

* Replace constant DS with actual slashes

#### 0.2.0 [2018-12-01]

* Add Basic Bootstrap Compiler (beginning with color definitions)
* Add Bootstrap JS
* Add debug position below footer
* Add template images
* Version
* Add FontAwesome 4.7.0

---
### 0.1

#### 0.1.4 [2018-11-30]

* Updater still doesn't seem to work

#### 0.1.3 [2018-11-30]

* Updater doesn't seem to work

#### 0.1.2 [2018-11-30]

* Version update
* Adjust some default values

#### 0.1.1 [2018-11-30]

* Version update
* Forgot to include files in templateDetails.xml, Fixes #1

#### 0.1.0 [2018-11-30]

* Add generic favicon
* German translation
* Add Basic Grid
* Add update server
* Add index.php
* Add languages (en-GB & de-DE)
* Add basic templateDetails.xml
* Initial commit (README & CHANGELOG)
