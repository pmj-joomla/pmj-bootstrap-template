/* based upon https://stackoverflow.com/questions/51185294/dropdown-menu-does-go-out-off-browser-screen-bootstrap-4 */
jQuery(document).ready(function() {
	jQuery(function () {
		jQuery(".dropdown").on('shown.bs.dropdown hidden.bs.dropdown', function (e) {
			if (jQuery('.dropdown-menu', this).length) {
				var elm = jQuery('.dropdown-menu', this);
				var off = elm.offset();
				var l = off.left;
				var w = elm.width();
				var docH = jQuery("body").height();
				var docW = jQuery("body").width();

				var isEntirelyVisible = (l + w <= docW);

				if (!isEntirelyVisible) {
					jQuery('.dropdown-menu', this).addClass('dropdown-menu-right');
				} else {
					jQuery('.dropdown-menu', this).removeClass('dropdown-menu-right');
				}
			}
		});
	});
});