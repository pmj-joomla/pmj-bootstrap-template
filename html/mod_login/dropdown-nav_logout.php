<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_login
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
?>
<ul class="navbar-nav<?php echo $params['moduleclass_sfx']; ?>">
	<li class="nav-item dropdown">
		<a class="nav-link dropdown-toggle" href="#" id="logoutDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		<?php if (!$params->get('name', 0)) : ?>
			<?php echo htmlspecialchars($user->get('name'), ENT_COMPAT, 'UTF-8'); ?>
		<?php else : ?>
			<?php echo htmlspecialchars($user->get('username'), ENT_COMPAT, 'UTF-8'); ?>
		<?php endif; ?>
		</a>
		<div class="dropdown-menu flex-column" aria-labelledby="logoutDropdown">
			<div class="mx-3 my-1">
				<?php if ($params->get('profilelink', 0)) : ?>
					<a class="dropdown-item pl-1" href="<?php echo JRoute::_('index.php?option=com_users&view=profile'); ?>">
						<?php echo JText::_('MOD_LOGIN_PROFILE'); ?>
					</a>
				<?php endif; ?>
				<?php $myModule = JModuleHelper::getModules('usermenu-navbar');
				if(!empty($myModule[0]))
				{
					echo JModuleHelper::renderModule($myModule[0]);
					echo '<div class="dropdown-divider"></div>';
				}
				?>
				<form action="<?php echo JRoute::_('index.php', true, $params->get('usesecure', 0)); ?>" method="post" id="login-form" class="form-vertical">
					<div class="logout-button">
						<input type="submit" name="Submit" class="btn btn-primary btn-block" value="<?php echo JText::_('JLOGOUT'); ?>" />
						<input type="hidden" name="option" value="com_users" />
						<input type="hidden" name="task" value="user.logout" />
						<input type="hidden" name="return" value="<?php echo $return; ?>" />
						<?php echo JHtml::_('form.token'); ?>
					</div>
				</form>
			</div>
		</div>
	</li>
</ul>