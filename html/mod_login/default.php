<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_login
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JLoader::register('UsersHelperRoute', JPATH_SITE . '/components/com_users/helpers/route.php');

JHtml::_('behavior.keepalive');

?>
<form action="<?php echo JRoute::_('index.php', true, $params->get('usesecure', 0)); ?>" method="post" id="login-form" class="">
	<?php if ($params->get('pretext')) : ?>
		<div class="pretext">
			<?php echo $params->get('pretext'); ?>
		</div>
	<?php endif; ?>
	
	<?php if (!$params->get('usetext', 0)) : ?>
		<div class="input-group mb-3">
			<div class="input-group-prepend">
				<span class="input-group-text" id="username-addon" data-toggle="tooltip" data-placement="top" title="<?php echo JText::_('MOD_LOGIN_VALUE_USERNAME'); ?>"><i class="fa fa-user"></i></span>
			</div>
			<input id="modlgn-username" type="text" name="username" class="form-control" tabindex="0" size="18" placeholder="<?php echo JText::_('MOD_LOGIN_VALUE_USERNAME'); ?>" aria-label="<?php echo JText::_('MOD_LOGIN_VALUE_USERNAME'); ?>" aria-describedby="username-addon" />
		</div>
	<?php else : ?>
		<div class="form-group">
			<label for="modlgn-username"><?php echo JText::_('MOD_LOGIN_VALUE_USERNAME'); ?></label>
			<input id="modlgn-username" type="text" name="username" class="form-control" placeholder="<?php echo JText::_('MOD_LOGIN_VALUE_USERNAME'); ?>">
		</div>
	<?php endif; ?>
	
	<?php if (!$params->get('usetext', 0)) : ?>
		<div class="input-group mb-3">
			<div class="input-group-prepend">
				<span class="input-group-text" id="password-addon" data-toggle="tooltip" data-placement="top" title="<?php echo JText::_('JGLOBAL_PASSWORD'); ?>"><i class="fa fa-lock"></i></span>
			</div>
			<input id="modlgn-passwd" type="password" name="password" class="form-control" tabindex="0" size="18" placeholder="<?php echo JText::_('JGLOBAL_PASSWORD'); ?>" aria-label="<?php echo JText::_('JGLOBAL_PASSWORD'); ?>" aria-describedby="password-addon" />
		</div>
	<?php else : ?>
		<div class="form-group">
			<label for="modlgn-passwd"><?php echo JText::_('JGLOBAL_PASSWORD'); ?></label>
			<input id="modlgn-passwd" type="password" name="password" class="form-control" placeholder="<?php echo JText::_('JGLOBAL_PASSWORD'); ?>">
		</div>
	<?php endif; ?>
	
	<?php if (count($twofactormethods) > 1) : ?>
		<?php if (!$params->get('usetext', 0)) : ?>
			<div class="input-group mb-3">
				<div class="input-group-prepend">
					<span class="input-group-text" id="secretkey-addon" data-toggle="tooltip" data-placement="top" title="<?php echo JText::_('JGLOBAL_SECRETKEY'); ?>"><i class="fa fa-star"></i></span>
				</div>
				<input id="modlgn-secretkey" type="text" name="secretkey" autocomplete="off" class="form-control" tabindex="0" size="18" placeholder="<?php echo JText::_('JGLOBAL_SECRETKEY'); ?>" aria-label="<?php echo JText::_('JGLOBAL_SECRETKEY'); ?>" aria-describedby="secretkey-addon" />
				<div class="input-group-append">
					<span class="input-group-text" id="secretkey-addon2" data-toggle="tooltip" data-placement="top" title="<?php echo JText::_('JGLOBAL_SECRETKEY_HELP'); ?>"><i class="fa fa-info"></i></span>
				</div
			</div>
		<?php else : ?>
			<div class="form-group">
				<label for="modlgn-secretkey"><?php echo JText::_('JGLOBAL_SECRETKEY'); ?></label>
				<input id="modlgn-secretkey" type="text" name="secretkey" autocomplete="off" class="form-control" placeholder="<?php echo JText::_('JGLOBAL_SECRETKEY'); ?>">
			</div>
		<?php endif; ?>
	<?php endif; ?>
	
	<?php if (JPluginHelper::isEnabled('system', 'remember')) : ?>
		<div class="input-group mt-3">
		<div id="form-login-remember" class="custom-control custom-checkbox mb-2">
			<input id="modlgn-remember" type="checkbox" name="remember" class="custom-control-input" value="yes">
			<label class="custom-control-label" for="modlgn-remember"><?php echo JText::_('MOD_LOGIN_REMEMBER_ME'); ?></label>
		</div>
		</div>
	<?php endif; ?>
	<div class="input-group">
		<button type="submit" tabindex="0" name="Submit" class="btn btn-primary"><?php echo JText::_('JLOGIN'); ?></button>
	</div>
	
	<?php $usersConfig = JComponentHelper::getParams('com_users'); ?>
	<ul class="nav flex-column">
	<?php if ($usersConfig->get('allowUserRegistration')) : ?>
		<li class="nav-item">
			<a class="nav-link pl-0" href="<?php echo JRoute::_('index.php?option=com_users&view=registration'); ?>">
				<?php echo JText::_('MOD_LOGIN_REGISTER'); ?>
			</a>
		</li>
	<?php endif; ?>
		<li class="nav-item">
			<a class="nav-link pl-0" href="<?php echo JRoute::_('index.php?option=com_users&view=remind'); ?>">
				<?php echo JText::_('MOD_LOGIN_FORGOT_YOUR_USERNAME'); ?>
			</a>
		</li>
		<li class="nav-item">
			<a class="nav-link pl-0" href="<?php echo JRoute::_('index.php?option=com_users&view=reset'); ?>">
				<?php echo JText::_('MOD_LOGIN_FORGOT_YOUR_PASSWORD'); ?>
			</a>
		</li>
	</ul>
	<input type="hidden" name="option" value="com_users" />
	<input type="hidden" name="task" value="user.login" />
	<input type="hidden" name="return" value="<?php echo $return; ?>" />
	<?php echo JHtml::_('form.token'); ?>
	
	<?php if ($params->get('posttext')) : ?>
		<div class="posttext">
			<?php echo $params->get('posttext'); ?>
		</div>
	<?php endif; ?>
	
</form>
