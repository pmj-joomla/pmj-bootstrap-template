<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_menu
 *
 * @copyright   Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$attributes = array();
// screenreader
$screenreader	= '';
$attributes['class'] = 'nav-link ';
if (in_array($item->id, $path))
{
	$attributes['class'] .= 'active ';
	$screenreader = ' <span class="sr-only">('.JText::_('MOD_MENU_NAVBAR_ACTIVE').')</span>';
}
elseif ($item->type === 'alias')
{
	$aliasToId = $item->params->get('aliasoptions');

	if (count($path) > 0 && $aliasToId == $path[count($path) - 1])
	{
		$attributes['class'] .= 'active ';
		$screenreader = ' <span class="sr-only">('.JText::_('MOD_MENU_NAVBAR_ACTIVE').')</span>';
	}
	elseif (in_array($aliasToId, $path))
	{
		$attributes['class'] .= 'alias-parent-active ';
	}
}

if ($item->anchor_title)
{
	$attributes['title'] = $item->anchor_title;
}

if ($item->anchor_css)
{
	if (strpos($item->anchor_css, 'fa ') !== false)
	{
		$item->title	= '<i class="'.$item->anchor_css.'"></i>';
	}
	else
	{
		$attributes['class'] = $attributes['class'].' '.$item->anchor_css;
	}
}

if ($item->anchor_rel)
{
	$attributes['rel'] = $item->anchor_rel;
}

$linktype = $item->title;

if ($item->menu_image)
{
	if ($item->menu_image_css)
	{
		$image_attributes['class'] = $item->menu_image_css;
		$linktype = JHtml::_('image', $item->menu_image, $item->title, $image_attributes);
	}
	else
	{
		$linktype = JHtml::_('image', $item->menu_image, $item->title);
	}

	if ($item->params->get('menu_text', 1))
	{
		$linktype .= '<span class="image-title">' . $item->title . '</span>';
	}
}

if ($item->browserNav == 1)
{
	$attributes['target'] = '_blank';
}
elseif ($item->browserNav == 2)
{
	$options = 'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes';

	$attributes['onclick'] = "window.open(this.href, 'targetWindow', '" . $options . "'); return false;";
}

//screenreader
$linktype	.= $screenreader;
echo JHtml::_('link', JFilterOutput::ampReplace(htmlspecialchars($item->flink, ENT_COMPAT, 'UTF-8', false)), $linktype, $attributes);
