<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_menu
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$id = '';

if ($tagId = $params->get('tag_id', ''))
{
	$id = ' id="' . $tagId . '"';
}
?>
<!--<ul class="<?php echo $class_sfx; ?>nav"<?php echo $id; ?>>-->
<?php
foreach ($list as $i => &$item)
{
	
	if ($item->level > 1) continue;
	
	$class = 'nav-item item-' . $item->id;

	//echo $item->deeper ? '<li class="' . $class . ' dropdown">' : ($item->parent_id === '1' ? '<li class="' . $class . '">' : '');

	switch ($item->type) :
		case 'separator':
		case 'component':
		case 'heading':
		case 'url':
			require JModuleHelper::getLayoutPath('mod_menu', 'pmj-navbar-usermenu_' . $item->type);
			break;

		default:
			require JModuleHelper::getLayoutPath('mod_menu', 'pmj-navbar-usermenu_url');
			break;
	endswitch;

	// The next item is deeper.
	/*if ($item->deeper)
	{
		echo '<div class="dropdown-menu">';
	}
	// The next item is shallower.
	elseif ($item->shallower)
	{
		echo $item->parent_id === '1' ? '</div>' : '</div></li>';
	}
	// The next item is on the same level.
	else
	{
		echo $item->parent_id === '1' ? '</li>' : '';
	}*/
}
?>
<!--</ul>-->
