<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_menu
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$title      = $item->anchor_title ? ' title="' . $item->anchor_title . '"' : '';
$anchor_css = $item->anchor_css ?: '';
$menu = JFactory::getApplication()->getMenu();
$parent = $menu->getActive()->parent_id;

if ($item->id == $parent)
{
  $anchor_css .= ' active';
}

$linktype   = $item->title;

if ($item->menu_image)
{
	if ($item->menu_image_css)
	{
		$image_attributes['class'] = $item->menu_image_css;
		$linktype = JHtml::_('image', $item->menu_image, $item->title, $image_attributes);
	}
	else
	{
		$linktype = JHtml::_('image', $item->menu_image, $item->title);
	}

	if ($item->params->get('menu_text', 1))
	{
		$linktype .= '<span class="image-title">' . $item->title . '</span>';
	}
}

$attributes	= '';
if ($item->deeper)
{
	$attributes	.= 'data-toggle="dropdown"';
	$attributes	.= ' role="button"';
	$attributes	.= ' aria-haspopup="true"';
	$attributes	.= ' aria-expanded="false"';
}

?>
<?php if ($item->deeper) : ?>
<a class="nav-link dropdown-toggle <?php echo $anchor_css; ?>" href="#"<?php echo $title; ?> <?php echo $attributes; ?>><?php echo $linktype; ?></a>
<?php endif; ?>
