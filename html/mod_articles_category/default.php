<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_category
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

?>
<?php if ($grouped) : ?>
	<?php foreach ($list as $group_name => $group) : ?>
		<div class="card my-2">
			<div class="card-header font-weight-bold">
				<?php echo JText::_($group_name); ?>
			</div>
			<ul class="list-group list-group-flush">
			<?php foreach ($group as $item) : ?>
				<li class="list-group-item px-2">
					<div>
					<?php if ($params->get('link_titles') == 1) : ?>
						<a class="mod-articles-category-title <?php echo $item->active; ?>" href="<?php echo $item->link; ?>">
							<?php echo $item->title; ?>
						</a>
					<?php else : ?>
						<?php echo $item->title; ?>
					<?php endif; ?>
					
					<?php if ($item->displayHits) : ?>
						<small class="badge badge-secondary">
							<?php echo $item->displayHits; ?>
						</small>
					<?php endif; ?>
					
					<?php if ($params->get('show_author')) : ?>
						<br /><small class="mod-articles-category-writtenby">
							<i class="fa fa-newspaper-o"></i> <?php echo $item->displayAuthorName; ?>
						</small>
					<?php endif; ?>
					</div>
					
					<?php if ($params->get('show_introtext')) : ?><div class="border-top border-bottom border-secondary py-1"><?php endif; ?>
					<?php if ($params->get('show_introtext')) : ?>
						<div class="mod-articles-category-introtext">
							<?php echo $item->displayIntrotext; ?>
						</div>
					<?php endif; ?>
					
					<?php if ($params->get('show_readmore')) : ?>
						<div class="mod-articles-category-readmore">
							<a class="btn btn-outline-primary btn-sm mod-articles-category-title <?php echo $item->active; ?>" href="<?php echo $item->link; ?>">
								<?php if ($item->params->get('access-view') == false) : ?>
									<?php echo JText::_('MOD_ARTICLES_CATEGORY_REGISTER_TO_READ_MORE'); ?>
								<?php elseif ($readmore = $item->alternative_readmore) : ?>
									<?php echo $readmore; ?>
									<?php echo JHtml::_('string.truncate', $item->title, $params->get('readmore_limit')); ?>
										<?php if ($params->get('show_readmore_title', 0) != 0) : ?>
											<?php echo JHtml::_('string.truncate', $item->title, $params->get('readmore_limit')); ?>
										<?php endif; ?>
								<?php elseif ($params->get('show_readmore_title', 0) == 0) : ?>
									<?php echo JText::sprintf('MOD_ARTICLES_CATEGORY_READ_MORE_TITLE'); ?>
								<?php else : ?>
									<?php echo JText::_('MOD_ARTICLES_CATEGORY_READ_MORE'); ?>
									<?php echo JHtml::_('string.truncate', $item->title, $params->get('readmore_limit')); ?>
								<?php endif; ?>
							</a>
						</div>
					<?php endif; ?>
					<?php if ($params->get('show_introtext')) : ?></div><?php endif; ?>
					
					<?php if ($item->displayCategoryTitle) : ?>
						<small class="mod-articles-category-category">
							(<?php echo $item->displayCategoryTitle; ?>)
						</small>
					<?php endif; ?>
					<?php if ($item->displayDate) : ?>
						<br /><small class="mod-articles-category-date"><?php echo $item->displayDate; ?></small>
					<?php endif; ?>
					<?php if ($params->get('show_tags', 0) && $item->tags->itemTags) : ?>
						<small class="mod-articles-category-tags">
							<?php echo JLayoutHelper::render('joomla.content.tags', $item->tags->itemTags); ?>
						</small>
					<?php endif; ?>
				</li>
			<?php endforeach; ?>
			</ul>
		</div>
	<?php endforeach; ?>
<?php else : ?>
	<?php if (!$params->get('show_introtext')) : ?><ul class="list-unstyled"><?php endif; ?>
	<?php foreach ($list as $item) : ?>
		<?php if ($params->get('show_introtext')) : ?>
		<div class="card my-2">
			<div class="card-header px-2">
				<?php if ($params->get('link_titles') == 1) : ?>
					<a class="mod-articles-category-title <?php echo $item->active; ?>" href="<?php echo $item->link; ?>"><?php echo $item->title; ?></a>
				<?php else : ?>
					<?php echo $item->title; ?>
				<?php endif; ?>
				
				<?php if ($item->displayHits) : ?>
					<small class="badge badge-secondary">
						<?php echo $item->displayHits; ?>
					</small>
				<?php endif; ?>
				
				<?php if ($params->get('show_author')) : ?>
					<br /><small class="mod-articles-category-writtenby">
						<i class="fa fa-newspaper-o"></i> <?php echo $item->displayAuthorName; ?>
					</small>
				<?php endif; ?>
			</div>
			
			<div class="card-body">
				<?php if ($params->get('show_introtext')) : ?>
					<span class="mod-articles-category-introtext">
						<?php echo $item->displayIntrotext; ?>
					</span>
				<?php endif; ?>
				
				<?php if ($params->get('show_readmore')) : ?>
					<div class="mod-articles-category-readmore">
						<a class="btn btn-outline-primary btn-sm mod-articles-category-title <?php echo $item->active; ?>" href="<?php echo $item->link; ?>">
							<?php if ($item->params->get('access-view') == false) : ?>
								<?php echo JText::_('MOD_ARTICLES_CATEGORY_REGISTER_TO_READ_MORE'); ?>
							<?php elseif ($readmore = $item->alternative_readmore) : ?>
								<?php echo $readmore; ?>
								<?php echo JHtml::_('string.truncate', $item->title, $params->get('readmore_limit')); ?>
									<?php if ($params->get('show_readmore_title', 0) != 0) : ?>
										<?php echo JHtml::_('string.truncate', $item->title, $params->get('readmore_limit')); ?>
									<?php endif; ?>
							<?php elseif ($params->get('show_readmore_title', 0) == 0) : ?>
								<?php echo JText::sprintf('MOD_ARTICLES_CATEGORY_READ_MORE_TITLE'); ?>
							<?php else : ?>
								<?php echo JText::_('MOD_ARTICLES_CATEGORY_READ_MORE'); ?>
								<?php echo JHtml::_('string.truncate', $item->title, $params->get('readmore_limit')); ?>
							<?php endif; ?>
						</a>
					</div>
				<?php endif; ?>
			</div>
			
			<div class="card-footer px-2 py-0">
				<?php if ($item->displayCategoryTitle) : ?>
					<small class="mod-articles-category-category">
						(<?php echo $item->displayCategoryTitle; ?>)
					</small>
				<?php endif; ?>
				<?php if ($item->displayDate) : ?>
					<br /><small class="mod-articles-category-date"><?php echo $item->displayDate; ?></small>
				<?php endif; ?>
				<?php if ($params->get('show_tags', 0) && $item->tags->itemTags) : ?>
					<small class="mod-articles-category-tags">
						<?php echo JLayoutHelper::render('joomla.content.tags', $item->tags->itemTags); ?>
					</small>
				<?php endif; ?>
			</div>
		</div>
		<?php else : ?>
		<li class="border-top border-dark py-1 my-2">
			<?php if ($params->get('link_titles') == 1) : ?>
				<a class="mod-articles-category-title <?php echo $item->active; ?>" href="<?php echo $item->link; ?>"><?php echo $item->title; ?></a>
			<?php else : ?>
				<?php echo $item->title; ?>
			<?php endif; ?>
			
			<?php if ($item->displayHits) : ?>
				<small class="badge badge-secondary">
					<?php echo $item->displayHits; ?>
				</small>
			<?php endif; ?>
			
			<?php if ($params->get('show_author')) : ?>
				<br /><small class="mod-articles-category-writtenby">
					<i class="fa fa-newspaper-o"></i> <?php echo $item->displayAuthorName; ?>
				</small>
			<?php endif; ?>
			
			<?php if ($item->displayCategoryTitle) : ?>
				<small class="mod-articles-category-category">
					(<?php echo $item->displayCategoryTitle; ?>)
				</small>
			<?php endif; ?>
			<?php if ($item->displayDate) : ?>
				<br /><small class="mod-articles-category-date"><?php echo $item->displayDate; ?></small>
			<?php endif; ?>
			<?php if ($params->get('show_tags', 0) && $item->tags->itemTags) : ?>
				<small class="mod-articles-category-tags">
					<?php echo JLayoutHelper::render('joomla.content.tags', $item->tags->itemTags); ?>
				</small>
			<?php endif; ?>
			
			<?php if ($params->get('show_readmore')) : ?>
				<div class="mod-articles-category-readmore pt-2">
					<a class="btn btn-outline-primary btn-sm mod-articles-category-title <?php echo $item->active; ?>" href="<?php echo $item->link; ?>">
						<?php if ($item->params->get('access-view') == false) : ?>
							<?php echo JText::_('MOD_ARTICLES_CATEGORY_REGISTER_TO_READ_MORE'); ?>
						<?php elseif ($readmore = $item->alternative_readmore) : ?>
							<?php echo $readmore; ?>
							<?php echo JHtml::_('string.truncate', $item->title, $params->get('readmore_limit')); ?>
								<?php if ($params->get('show_readmore_title', 0) != 0) : ?>
									<?php echo JHtml::_('string.truncate', $item->title, $params->get('readmore_limit')); ?>
								<?php endif; ?>
						<?php elseif ($params->get('show_readmore_title', 0) == 0) : ?>
							<?php echo JText::sprintf('MOD_ARTICLES_CATEGORY_READ_MORE_TITLE'); ?>
						<?php else : ?>
							<?php echo JText::_('MOD_ARTICLES_CATEGORY_READ_MORE'); ?>
							<?php echo JHtml::_('string.truncate', $item->title, $params->get('readmore_limit')); ?>
						<?php endif; ?>
					</a>
				</div>
			<?php endif; ?>
		</li>
		<?php endif; ?>
	<?php endforeach; ?>
	<?php if (!$params->get('show_introtext')) : ?></ul><?php endif; ?>
<?php endif; ?>