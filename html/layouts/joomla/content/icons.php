<?php
/**
 * @package     Joomla.Site
 * @subpackage  Layout
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('JPATH_BASE') or die;

$canEdit = $displayData['params']->get('access-edit');
$articleId = $displayData['item']->id;

?>

<div class="icons">
	<?php if (empty($displayData['print'])) : ?>

		<?php if ($canEdit || $displayData['params']->get('show_print_icon') || $displayData['params']->get('show_email_icon')) : ?>
			<div class="btn-group float-right" role="group">
				<button id="iconGroupDrop<?php echo $articleId; ?>" type="button" class="btn btn-outline-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" aria-label="<?php echo JText::_('JUSER_TOOLS'); ?>">
					<i class="fa fa-cog"></i>
				</button>
				<div class="dropdown-menu dropdown-menu-right" aria-labelledby="iconGroupDrop<?php echo $articleId; ?>">
					<?php if ($displayData['params']->get('show_print_icon')) : ?>
						<div class="dropdown-item"><?php echo JHtml::_('icon.print_popup', $displayData['item'], $displayData['params']); ?></div>
					<?php endif; ?>
					<?php if ($displayData['params']->get('show_email_icon')) : ?>
						<div class="dropdown-item"><?php echo JHtml::_('icon.email', $displayData['item'], $displayData['params']); ?></div>
					<?php endif; ?>
					<?php if ($canEdit) : ?>
						<div class="dropdown-item"><?php echo JHtml::_('icon.edit', $displayData['item'], $displayData['params']); ?></div>
					<?php endif; ?>
				</div>
			</div>
		<?php endif; ?>

	<?php else : ?>

		<div class="float-right">
			<?php echo JHtml::_('icon.print_screen', $displayData['item'], $displayData['params']); ?>
		</div>

	<?php endif; ?>
</div>
