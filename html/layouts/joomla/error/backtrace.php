<?php
defined('_JEXEC') or die;

/** @var $displayData array */
$backtraceList = $displayData['backtrace'];

if (!$backtraceList)
{
	return;
}

$class = isset($displayData['class']) ? $displayData['class'] : 'table table-striped table-bordered';
?>
<table class="table table-sm table-responsive">
	<thead class="thead-dark">
		<tr>
			<th>
				<strong>#</strong>
			</th>
			<th>
				<strong>Function</strong>
			</th>
			<th>
				<strong>Location</strong>
			</th>
		</tr>
	</thead>
	
	<tbody>
		<?php foreach ($backtraceList as $k => $backtrace): ?>
		<tr>
			<td>
				<?php echo $k + 1; ?>
			</td>

			<?php if (isset($backtrace['class'])): ?>
			<td>
				<?php echo $backtrace['class'] . $backtrace['type'] . $backtrace['function'] . '()'; ?>
			</td>
			<?php else: ?>
			<td>
				<?php echo $backtrace['function'] . '()'; ?>
			</td>
			<?php endif; ?>

			<?php if (isset($backtrace['file'])): ?>
			<td>
				<?php echo JHtml::_('debug.xdebuglink', $backtrace['file'], $backtrace['line']); ?>
			</td>
			<?php else: ?>
			<td>
				&#160;
			</td>
			<?php endif; ?>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>
