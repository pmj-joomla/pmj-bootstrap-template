<?php
defined('_JEXEC') or die;

function modChrome_hcard( $module, &$params, &$attribs )
{
  $headerTag     = htmlspecialchars($params->get('header_tag', 'h5'), ENT_QUOTES, 'UTF-8');
  
  // attributes
  $card_border_color  = $attribs['card_border_color'] ? $attribs['card_border_color'] : 'default';
  $card_text_color  = $attribs['card_text_color'] ? $attribs['card_text_color'] : 'dark';
  $card_bg_color  = $attribs['card_bg_color'] ? $attribs['card_bg_color'] : 'light';
  $card_header_bg_color = $attribs['card_header_bg_color'] ? $attribs['card_header_bg_color'] : 'default';
  $card_header_text_color = $attribs['card_header_text_color'] ? $attribs['card_header_text_color'] : 'dark';
  
  if ($module->content)
  {
    ?>
    <div class="card border-<?php echo $card_border_color; ?> text-<?php echo $card_text_color; ?> bg-<?php echo $card_bg_color; ?> my-2">
      <?php if ($module->showtitle) : ?>
      <div class="card-header bg-<?php echo $card_header_bg_color; ?> text-<?php echo $card_header_text_color; ?>">
        <<?php echo $headerTag; ?>><?php echo $module->title; ?></<?php echo $headerTag; ?>>
      </div>
      <?php endif; ?>
      <div class="card-body">
        <?php echo $module->content; ?>
      </div>
    </div>
    <?php
  }
}

function modChrome_card( $module, &$params, &$attribs )
{
  $headerTag     = htmlspecialchars($params->get('header_tag', 'h5'), ENT_QUOTES, 'UTF-8');
  
  // attributes
  $card_border_color  = $attribs['card_border_color'] ? $attribs['card_border_color'] : 'default';
  $card_text_color  = $attribs['card_text_color'] ? $attribs['card_text_color'] : 'dark';
  $card_bg_color  = $attribs['card_bg_color'] ? $attribs['card_bg_color'] : 'light';
  $card_header_bg_color = $attribs['card_header_bg_color'] ? $attribs['card_header_bg_color'] : 'default';
  $card_header_text_color = $attribs['card_header_text_color'] ? $attribs['card_header_text_color'] : 'dark';
  
  if ($module->content)
  {
    ?>
    <div class="card border-<?php echo $card_border_color; ?> text-<?php echo $card_text_color; ?> bg-<?php echo $card_bg_color; ?> my-2">
      <div class="card-body">
        <?php if ($module->showtitle) : ?>
        <<?php echo $headerTag; ?> class="card-title bg-<?php echo $card_header_bg_color; ?> text-<?php echo $card_header_text_color; ?>"><?php echo $module->title; ?></<?php echo $headerTag; ?>>
        <?php endif; ?>
        <?php echo $module->content; ?>
      </div>
    </div>
    <?php
  }
}